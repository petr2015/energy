<footer class="footer">
    <div class="indent">
        <div class="flex-js-fw inf-f">
            <div class="box-f">
                <div>
                    Екатеринбург: +7 (343) 382-23-52<br>
                    Челябинск: +7 (351) 751-28-06<br>
                    Нижний Тагил: +7 (922) 171-31-23<br>
                </div>
            </div>
            <div class="box-f">
                <div>
                    Тюмень: +7 (3452) 60-84-52<br>
                    Курган: +7 (3522) 66-29-82<br>
                    Магнитогорск: +7 (922) 016-23-60<br>
                </div>
            </div>
            <div class="box-f">
                <div>
                    Уфа: +7 (927) 236-00-24<br>
                    Пермь: +7 (342) 204-62-75<br>
                </div>
            </div>
            <div class="box-f">
                <div>
                    Сургут: +7 (932) 402-58-83<br>
                    Нижневартовск: +7 (3466) 21-98-83<br>
                </div>
            </div>
        </div>
        <div class="flex-js-fw bottom-f">
            <div class="box-f">
                <div>© Энергомир 2008 - 2019</div>
                <div>ВСЕ ПРАВА ЗАЩИЩЕНЫ</div>
                <div>Профессиональная разработка сложных веб сайтов <a href="#">WMStudio</a></div>
            </div>
            <div class="">
                <div>МЫ ПРИНиМАЕМ:</div>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="/assets/js/script.min.js"></script>
<script src="//ulogin.ru/js/ulogin.js"></script>
</body>
</html>