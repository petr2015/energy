<aside class="sidebar">
    <button class="btn width green sidebarBtn-click">Выбор по параметрам
        <i class="fa fa-check-square-o" aria-hidden="true"></i>
    </button>
    <form action="#" class="sidebarForm">
        <div class="filter-option">
            <div class="option-name1 mb1">
                <span class="text">Цена (Р)</span>
            </div>
            <div class="filter-open1">
                <div class="wrap-slider-range">
                    <div class="slider-range">
                        <input type="text" id="prise">
                        <input type="text" id="prise2">
                    </div>
                    <div id="slider-prise"></div>
                </div>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Производитель</span>
            </div>
            <div class="filter-open">
                <label for="filter" class="checkboxLabel">
                    <input type="checkbox" id="filter" name="filter">
                    <a href="#" class="checkboxLabel-link">9Bar</a>
                </label>
                <label for="filter1" class="checkboxLabel">
                    <input type="checkbox" id="filter1" name="filter">
                    <a href="#" class="checkboxLabel-link">ACV</a>
                </label>
                <label for="filter2" class="checkboxLabel">
                    <input type="checkbox" id="filter2" name="filter">
                    <a href="#" class="checkboxLabel-link">Arderia</a>
                </label>
                <label for="filter3" class="checkboxLabel">
                    <input type="checkbox" id="filter3" name="filter">
                    <a href="#" class="checkboxLabel-link">Austria Email</a>
                </label>
                <label for="filter4" class="checkboxLabel">
                    <input type="checkbox" id="filter4" name="filter">
                    <a href="#" class="checkboxLabel-link">Baxi</a>
                </label>
                <label for="filter5" class="checkboxLabel">
                    <input type="checkbox" id="filter5" name="filter">
                    <a href="#" class="checkboxLabel-link">Bosch</a>
                </label>
                <label for="filter6" class="checkboxLabel">
                    <input type="checkbox" id="filter6" name="filter">
                    <a href="#" class="checkboxLabel-link">Buderus</a>
                </label>
                <label for="filter7" class="checkboxLabel">
                    <input type="checkbox" id="filter7" name="filter">
                    <a href="#" class="checkboxLabel-link">Drazice</a>
                </label>
                <label for="filter8" class="checkboxLabel">
                    <input type="checkbox" id="filter8" name="filter">
                    <a href="#" class="checkboxLabel-link">Ferroli</a>
                </label>
                <label for="filter9" class="checkboxLabel">
                    <input type="checkbox" id="filter9" name="filter">
                    <a href="#" class="checkboxLabel-link">Galmet</a>
                </label>
                <label for="filter10" class="checkboxLabel">
                    <input type="checkbox" id="filter10" name="filter">
                    <a href="#" class="checkboxLabel-link">Gorenje</a>
                </label>
                <label for="filter11" class="checkboxLabel">
                    <input type="checkbox" id="filter11" name="filter">
                    <a href="#" class="checkboxLabel-link">Hajdu</a>
                </label>
                <label for="filter12" class="checkboxLabel">
                    <input type="checkbox" id="filter12" name="filter">
                    <a href="#" class="checkboxLabel-link">Kospel</a>
                </label>
                <label for="filter13" class="checkboxLabel">
                    <input type="checkbox" id="filter13" name="filter">
                    <a href="#" class="checkboxLabel-link">Lapesa</a>
                </label>
                <label for="filter14" class="checkboxLabel">
                    <input type="checkbox" id="filter14" name="filter">
                    <a href="#" class="checkboxLabel-link">Nibe</a>
                </label>
                <label for="filter15" class="checkboxLabel">
                    <input type="checkbox" id="filter15" name="filter">
                    <a href="#" class="checkboxLabel-link">Oso</a>
                </label>
                <label for="filter16" class="checkboxLabel">
                    <input type="checkbox" id="filter16" name="filter">
                    <a href="#" class="checkboxLabel-link">Protherm</a>
                </label>
                <label for="filter17" class="checkboxLabel">
                    <input type="checkbox" id="filter17" name="filter">
                    <a href="#" class="checkboxLabel-link">Vaillant</a>
                </label>
                <label for="filter18" class="checkboxLabel">
                    <input type="checkbox" id="filter18" name="filter">
                    <a href="#" class="checkboxLabel-link">Wester</a>
                </label>
                <div class="width">
                    <button class="btn fs12 white">Показать еще <i class="fa fa-level-down"
                                                                   aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Объем бака, л</span>
            </div>
            <div class="filter-open">
                <div class="wrap-slider-range">
                    <div class="slider-range">
                        <input type="text" id="amount">
                        <input type="text" id="amount2">
                    </div>
                    <div id="slider-range"></div>
                </div>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Монтаж</span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="installation" class="checkboxLabel">
                    <input type="checkbox" id="installation" name="installation">
                    <a href="#" class="checkboxLabel-link">напольный</a>
                </label>
                <label for="installation" class="checkboxLabel">
                    <input type="checkbox" id="installation" name="installation">
                    <a href="#" class="checkboxLabel-link">настенный</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Тип корпуса </span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="installation" class="checkboxLabel">
                    <input type="checkbox" id="installation" name="installation">
                    <a href="#" class="checkboxLabel-link">вертикальный</a>
                </label>
                <label for="installation" class="checkboxLabel">
                    <input type="checkbox" id="installation" name="installation">
                    <a href="#" class="checkboxLabel-link">горизонтальный</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Производительность, л/мин </span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <div class="wrap-slider-range">
                    <div class="slider-range">
                        <input type="text" id="performance">
                        <input type="text" id="performance2">
                    </div>
                    <div id="slider-performance"></div>
                </div>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Внутреннее покрытие бака</span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="cover" class="checkboxLabel">
                    <input type="checkbox" id="cover" name="cover">
                    <a href="#" class="checkboxLabel-link">нерж. сталь</a>
                </label>
                <label for="cover1" class="checkboxLabel">
                    <input type="checkbox" id="cover1" name="cover">
                    <a href="#" class="checkboxLabel-link">сталь</a>
                </label>
                <label for="cover2" class="checkboxLabel">
                    <input type="checkbox" id="cover2" name="cover">
                    <a href="#" class="checkboxLabel-link">стеклокерамика</a>
                </label>
                <label for="cover3" class="checkboxLabel">
                    <input type="checkbox" id="cover3" name="cover">
                    <a href="#" class="checkboxLabel-link">термоглазурь</a>
                </label>
                <label for="cover4" class="checkboxLabel">
                    <input type="checkbox" id="cover4" name="cover">
                    <a href="#" class="checkboxLabel-link">титановая эмаль</a>
                </label>
                <label for="cover5" class="checkboxLabel">
                    <input type="checkbox" id="cover5" name="cover">
                    <a href="#" class="checkboxLabel-link">эмал.сталь</a>
                </label>
                <label for="cover6" class="checkboxLabel">
                    <input type="checkbox" id="cover6" name="cover">
                    <a href="#" class="checkboxLabel-link">эмаль</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Магниевый анод </span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="magnet" class="checkboxLabel">
                    <input type="radio" id="magnet" name="magnet">
                    <a href="#" class="checkboxLabel-link">нерж. сталь</a>
                </label>
                <label for="magnet1" class="checkboxLabel">
                    <input type="radio" id="magnet1" name="magnet">
                    <a href="#" class="checkboxLabel-link">сталь</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Мощность тепловая, кВт</span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <div class="wrap-slider-range">
                    <div class="slider-range">
                        <input type="text" id="power">
                        <input type="text" id="power2">
                    </div>
                    <div id="slider-power"></div>
                </div>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Способ нагрева</span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="heating-method2" class="checkboxLabel">
                    <input type="radio" id="heating-method2" name="heating-method">
                    <a href="#" class="checkboxLabel-link">нерж. сталь</a>
                </label>
                <label for="heating-method" class="checkboxLabel">
                    <input type="radio" id="heating-method" name="heating-method">
                    <a href="#" class="checkboxLabel-link">сталь</a>
                </label>
                <label for="heating-method1" class="checkboxLabel">
                    <input type="radio" id="heating-method1" name="heating-method">
                    <a href="#" class="checkboxLabel-link">сталь</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Конструкция </span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="design" class="checkboxLabel">
                    <input type="radio" id="design" name="design">
                    <a href="#" class="checkboxLabel-link">Все</a>
                </label>
                <label for="design1" class="checkboxLabel">
                    <input type="radio" id="design1" name="design">
                    <a href="#" class="checkboxLabel-link">бак в баке</a>
                </label>
                <label for="design2" class="checkboxLabel">
                    <input type="radio" id="design2" name="design">
                    <a href="#" class="checkboxLabel-link">бак в баке и змеевик</a>
                </label>
                <label for="design3" class="checkboxLabel">
                    <input type="radio" id="design3" name="design">
                    <a href="#" class="checkboxLabel-link">змеевик</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Обратный клапан</span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="check-valve" class="checkboxLabel">
                    <input type="radio" id="check-valve" name="check-valve">
                    <a href="#" class="checkboxLabel-link">Все</a>
                </label>
                <label for="check-valve1" class="checkboxLabel">
                    <input type="radio" id="check-valve1" name="design">
                    <a href="#" class="checkboxLabel-link">есть</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Диаметр трубы ГВС, мм (дюйм) </span>
                <div class="tip">
                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                    <div class="tip-hov">
                        <div class="tip-tit">Подводка</div>
                        <div class="tip-text">Сторона прибора с которой производятся
                            подключение воды/ электричества/ газа
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-open">
                <label for="diameter" class="checkboxLabel">
                    <input type="checkbox" id="diameter" name="diameter">
                    <a href="#" class="checkboxLabel-link">15 [1/2"]</a>
                </label>
                <label for="diameter1" class="checkboxLabel">
                    <input type="checkbox" id="diameter1" name="diameter">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="diameter2" class="checkboxLabel">
                    <input type="checkbox" id="diameter2" name="diameter">
                    <a href="#" class="checkboxLabel-link">25 [1"]</a>
                </label>
                <label for="diameter3" class="checkboxLabel">
                    <input type="checkbox" id="diameter3" name="diameter">
                    <a href="#" class="checkboxLabel-link">32 [1 1/4"]</a>
                </label>
                <label for="diameter4" class="checkboxLabel">
                    <input type="checkbox" id="diameter4" name="diameter">
                    <a href="#" class="checkboxLabel-link">40 [1 1/2"]</a>
                </label>
                <label for="diameter5" class="checkboxLabel">
                    <input type="checkbox" id="diameter5" name="diameter">
                    <a href="#" class="checkboxLabel-link">50 [2"]</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Диаметр трубы отопления, мм (дюйм)</span>
            </div>
            <div class="filter-open">
                <label for="diameterD" class="checkboxLabel">
                    <input type="checkbox" id="diameterD" name="diameterD">
                    <a href="#" class="checkboxLabel-link">15 [1/2"]</a>
                </label>
                <label for="diameterD1" class="checkboxLabel">
                    <input type="checkbox" id="diameterD1" name="diameterD">
                    <a href="#" class="checkboxLabel-link">32 [1 1/4"]</a>
                </label>
                <label for="diameterD2" class="checkboxLabel">
                    <input type="checkbox" id="diameterD2" name="diameterD">
                    <a href="#" class="checkboxLabel-link">50 [2"]</a>
                </label>
                <label for="diameterD3" class="checkboxLabel">
                    <input type="checkbox" id="diameterD3" name="diameterD">
                    <a href="#" class="checkboxLabel-link">25 [1"]</a>
                </label>
                <label for="diameterD4" class="checkboxLabel">
                    <input type="checkbox" id="diameterD4" name="diameterD">
                    <a href="#" class="checkboxLabel-link">40 [1 1/2"]</a>
                </label>
                <label for="diameterD5" class="checkboxLabel">
                    <input type="checkbox" id="diameterD5" name="diameterD">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">ТЭН</span>
            </div>
            <div class="filter-open">
                <label for="tehs" class="checkboxLabel">
                    <input type="checkbox" id="tehs" name="tehs">
                    <a href="#" class="checkboxLabel-link">1,5 кВт</a>
                </label>
                <label for="tehs1" class="checkboxLabel">
                    <input type="checkbox" id="tehs1" name="tehs">
                    <a href="#" class="checkboxLabel-link">15 кВт</a>
                </label>
                <label for="tehs2" class="checkboxLabel">
                    <input type="checkbox" id="tehs2" name="tehs">
                    <a href="#" class="checkboxLabel-link">15 кВт/3х5кВт/</a>
                </label>
                <label for="tehs3" class="checkboxLabel">
                    <input type="checkbox" id="tehs3" name="tehs">
                    <a href="#" class="checkboxLabel-link">2 кВт</a>
                </label>
                <label for="tehs4" class="checkboxLabel">
                    <input type="checkbox" id="tehs4" name="tehs">
                    <a href="#" class="checkboxLabel-link">2,2 кВт</a>
                </label>
                <label for="tehs6" class="checkboxLabel">
                    <input type="checkbox" id="tehs6" name="tehs">
                    <a href="#" class="checkboxLabel-link">3 кВт</a>
                </label>
                <label for="tehs7" class="checkboxLabel">
                    <input type="checkbox" id="tehs7" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs8" class="checkboxLabel">
                    <input type="checkbox" id="tehs8" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs9" class="checkboxLabel">
                    <input type="checkbox" id="tehs9" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs11" class="checkboxLabel">
                    <input type="checkbox" id="tehs11" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs12" class="checkboxLabel">
                    <input type="checkbox" id="tehs12" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs13" class="checkboxLabel">
                    <input type="checkbox" id="tehs13" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs14" class="checkboxLabel">
                    <input type="checkbox" id="tehs14" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs15" class="checkboxLabel">
                    <input type="checkbox" id="tehs15" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs16" class="checkboxLabel">
                    <input type="checkbox" id="tehs16" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs17" class="checkboxLabel">
                    <input type="checkbox" id="tehs17" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <label for="tehs18" class="checkboxLabel">
                    <input type="checkbox" id="tehs18" name="tehs">
                    <a href="#" class="checkboxLabel-link">20 [3/4"]</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="filter-option">
            <div class="option-name">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
                <span class="text">Рабочее давление, бар (атм)</span>
            </div>
            <div class="filter-open">
                <label for="work" class="checkboxLabel">
                    <input type="checkbox" id="work" name="work">
                    <a href="#" class="checkboxLabel-link">0,6</a>
                </label>
                <label for="work1" class="checkboxLabel">
                    <input type="checkbox" id="work1" name="work">
                    <a href="#" class="checkboxLabel-link">10</a>
                </label>
                <label for="work2" class="checkboxLabel">
                    <input type="checkbox" id="work2" name="work">
                    <a href="#" class="checkboxLabel-link">6</a>
                </label>
                <button class="btn green width">Применить фильтр <i class="fa fa-check"
                                                                    aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="flex-jb-ac-fw pt10">
            <div class="fs10 btn green">Все параметры <i class="fa fa-level-down"
                                                         aria-hidden="true"></i></div>
            <div class="fs10 btn grey">Сбросить фильтр</div>

        </div>
    </form>
</aside>