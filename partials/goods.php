<div class="goods">
    <a href="#" class="goods-link bold"><span class="arrow"></span>БОЙЛЕРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ВОДОНАГРЕВАТЕЛИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ГОРЕЛКИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ИНФРАКРАСНЫЕ ОБОГРЕВАТЕЛИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КАЛОРИФЕРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КОНВЕКТОРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КОНДИЦИОНЕРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КОТЛЫ ОТОПЛЕНИЯ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>НАСОСЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ТЕПЛОВЫЕ ЗАВЕСЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ТЕПЛОВЫЕ ПУШКИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ТЕПЛОГЕНЕРАТОРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>АВТОМАТИКА</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ДЫМОХОДЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ЗАПЧАСТИ И КОМПЛЕКТУЮЩИЕ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КОМПЛЕКТУЮЩИЕ ОТОПИТЕЛЬНЫХ СИСТЕМ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>КОНДИЦИОНЕРЫ ПРОМЫШЛЕННЫЕ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ОСУШИТЕЛИ ВОЗДУХА</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ПРОМЫШЛЕННЫЕ УВЛАЖНИТЕЛИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>РАДИАТОРЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>РАСШИРИТЕЛЬНЫЕ БАКИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>СТАБИЛИЗАТОРЫ И ИБП</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>УСТАРЕВШИЕ ПОЗИЦИИ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ФАНКОЙЛЫ</a>
    <a href="#" class="goods-link bold"><span class="arrow"></span>ЭЛЕКТРОКАМИНЫ</a>

    <a href="#" class="goods-link mob"><span class="arrow"></span>Производители</a>
    <a href="#" class="goods-link mob"><span class="arrow"></span>Оплата и доставка</a>
    <a href="#" class="goods-link mob"><span class="arrow"></span>Наши работы</a>
    <a href="#" class="goods-link mob"><span class="arrow"></span>О компании</a>
    <a href="#" class="goods-link mob"><span class="arrow"></span>Статьи</a>
    <a href="#" class="goods-link mob"><span class="arrow"></span>Контакты</a>
</div>