<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/style.css">

</head>
<body>

<header class="header">
    <div class="indent">
        <div class="header-top">
            <div class="box-logo">
                <a href="/" class="logo">
                    <img src="/assets/img/logo.png" title="" alt="">
                </a>
                <a href="mailto:energomir@energomir.su" class="email-h">
                    <span class="email-ic"></span>
                    energomir@energomir.su
                </a>
            </div>
            <div class="menuPage">
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        ПРоиЗВОДИТЕЛИ
                    </a>
                </div>
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        ОПЛАТА И ДОСТАВКА
                    </a>
                </div>
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        НАШИ РАБОТЫ
                    </a>
                </div>
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        О КОМПАНИИ
                    </a>
                </div>
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        СТАТЬИ
                    </a>
                </div>
                <div class="menuPage-box">
                    <a href="#" class="menuPage-link">
                        <span class="arrow"></span>
                        КОНТАКТЫ
                    </a>
                </div>
            </div>
            <div class="region">
                <div class="text">
                    Ваш регион:
                    <a href="#" class="open-popup">Изменить</a>
                </div>
                <div class="text bold">
                    <span class="fa fa-map-marker fa-fw"></span>
                    Екатеринбург
                </div>
                <div class="text">
                    <span class="fa fa-clock-o fa-fw"></span>
                    Пн - Пт 8:00 - 20:00
                </div>
            </div>
            <div class="phone-h">
                <a href="tel:+7 (343) 382-23-52" class="phone">+7 (343) 382-23-52</a>
                <a href="tel:+79222027971" class="phone ">+7 (922) 202-79-71</a>
            </div>
            <div class="hamburger hamburger--spin">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="headerFixed">
        <div class="indent">
            <div class="headerFixed-wrap">
                <div class="wrapMenu "><!-- добавить класс 'open' только для главной стр-->
                    <div class="wrapMenu-absolute">
                        <div class="menu">
                            <span class="before"></span>
                            КАТАЛОГ ТОВАРОВ
                            <span class="arrow"></span>
                        </div>
                        <ul class="menu-open">
                            <li class="menu-li">
                                <a href="#" class="menu-link">Бойлеры <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Бойлеры<span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Буферные емкости</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Косвенного нагрева</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрические</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Водонагреватели <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Водонагреватели<span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрические</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Косвенного нагрева</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Горелки <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Горелки<span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дизельные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Комбинированные мультитопливные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Мазутные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">На обработанном масле</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Нефтяные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Пеллетные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Рампы и комплектующие</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Инфракрасные обогреватели</a>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Калориферы <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Калориферы <span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Отопительные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дестратификаторы</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Канальные</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Конвекторы <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Конвекторы<span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Встраиваемые внутрипольные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Напольные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрические</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Кондицилнеры <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Кондицилнеры <span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Настенные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Канальные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Кассетные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Колонные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Мобильные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Мульти-сплит</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Напольные/ потолочные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Оконные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Внешние блоки</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Котлы отопления <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Котлы отопления <span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые/ дизельные под сменную горелку</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дизельные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">На обработанном масле</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Паровые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Пеллетные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Промышленные водогрейные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Твердотопливные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Термомасляные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрические</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link-img">
                                            <img src="/assets/img/kotel-menu.png" title="" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Насосы <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Насосы<span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дренажные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Насосные станции</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Поверхностные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Погружные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Фекальные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Циркуляционные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Автоматика для систем водоснабжения</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Тепловые завесы</a>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Тепловые пушки <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link bold">Тепловые пушки <span class="arrow"></span></a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Газовые</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дизельные</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">На горячей воде</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрические</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Теплогенераторы</a>
                            </li>
                            <li class="menu-li">
                                <a href="#" class="menu-link">Еще <span class="arrow"></span></a>
                                <ul class="menu-hover">
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Автоматика</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Дымоходы<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link bold">Дымоходы<span class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Arderia</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Baxi</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Bosch</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Buderus</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Craft</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Daewoo</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Ferroli</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Hydrosta</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Kiturami</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Navien</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Protherm</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Еще...</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Запчасти и комплектующие<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Запчасти и комплектующие<span class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Запчасти</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Насосы топливные</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Блоки управления</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие для калориферов</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие для кондиционеров</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие для тепловых завес</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие к инфрокрассным обогревателям</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие радиаторов</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Форсунки</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие для конвекторов</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Комплектующие отопительных систем<span
                                                    class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комплектующие отопительных систем<span class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Арматура</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Антифриз</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Гидроаккумуляторы</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Группа безопасности</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Для котельных на обработанном масле</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Оборудование для подачи топлива</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Теплообменники</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Кондиционеры промышленные<span
                                                    class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Воздушные тепловые насосы<span class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Компрессорно-конденсаторные и внутренние блоки (ККБ)</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Мультизональные</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Прайс-листы</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Прецизионные</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Руфтопы (Крышные кондиционеры)</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Чиллеры</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Осушители воздуха<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Осушители воздуха<span
                                                            class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">дсорбционные</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Бытовые</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Для бассейна</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Канальные</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Промышленные</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Промышленные увлажнители</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Радиаторы</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Расширенные баки</a>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Стабилизаторы и ИБП <span
                                                    class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Стабилизаторы и ИБП<span
                                                            class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Защита от скачков напряжения</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Источники бесперебойного питания</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Стабилизаторы напряжения</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Устаревшие позиции<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Устаревшие позиции<span
                                                            class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Архивные модели</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Вентиляция</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Фанкойлы<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Фанкойлы<span class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Комлектующие для фанкойлов</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="menu-li">
                                        <a href="#" class="menu-link">Электрокамины<span class="arrow"></span></a>
                                        <ul class="menu-hover-yet">
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Электрокамины<span
                                                            class="arrow"></span></a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Alex Bauman</a>
                                            </li>
                                            <li class="menu-li">
                                                <a href="#" class="menu-link">Dimplex</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="search-h">
                    <div class="search">
                        <input type="text" placeholder="Поиск товаров">
                        <button type="submit" class="searchBtn">
                            <span class="mobNone">найти</span>
                            <span class="search-ic"></span>
                        </button>
                    </div>
                </div>
                <div class="basket">
                    <div class="basket-img">
                        <img src="/assets/img/basked.png" title="" alt="">
                    </div>
                    <div class="basket-text">
                        <div class="title">КОРЗИНА</div>
                        <a href="/page/basket.php" class="link"><span>ТОВАРОВ:</span> 1000 <span>(994440 Р.)</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>