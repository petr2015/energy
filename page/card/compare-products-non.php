<?php include '../../partials/header.php' ?>

    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li">Сравнение товаров</li>
            </ul>
            <div class="wrap-maxCard">
                <h3 class="bold">Сравнение товаров</h3>
                <div class="pd-mb">
                    <p>Вы еще не добавили товары в список сравнения.</p>
                </div>
            </div>
            <?php include '../../partials/goods.php' ?>
        </div>
    </div>

<?php include '../../partials/footer.php' ?>