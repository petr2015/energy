<?php include '../../partials/header.php' ?>

    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li">Сравнение товаров</li>
            </ul>
            <div class="wrap-maxCard">
                <h3 class="bold">Сравнение товаров</h3>
                <div class="propertyTab pt-bt">
                    <div class="property__caption">
                        <div class="property-link active">
                            <span class="fa fa-balance-scale" aria-hidden="true"></span>
                            <span class="dotted">Различающиеся</span>
                        </div>
                        <div class="property-link">
                            <span class="fa fa-list" aria-hidden="true"></span>
                            <span class="dotted">Все характеристики</span>
                        </div>
                    </div>
                    <div class="property__content active">
                        <div class="wrap-table flex-js-fw">
                            <div class="tableArrow tablePrev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </div>
                            <table class="tableName table1">
                                <tr data-height="1">
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Объем бака, л</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Производительность, л/мин
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                </tr>
                                <tr>
                                    <td class="bold">Вес без упаковки, кг</td>
                                </tr>
                                <tr>
                                    <td class="bold">Высота, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Глубина, мм</td>
                                </tr>
                            </table>
                            <div class="tableProperty-wrap">
                                <table class="table2 tableProperty">
                                    <tr>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">100</td>
                                        <td class="">140</td>
                                        <td class="">200</td>
                                        <td class="">300</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                    </tr>
                                    <tr>
                                        <td class="">8,5</td>
                                        <td class="">12,3</td>
                                        <td class="">17,8</td>
                                        <td class="">17,8</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                    </tr>
                                    <tr>
                                        <td class="">21,1</td>
                                        <td class="">30,4</td>
                                        <td class="">44,8</td>
                                        <td class="">44,8</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">60</td>
                                        <td class="">65</td>
                                        <td class="">84</td>
                                        <td class="">122</td>
                                        <td class="">147</td>
                                    </tr>
                                    <tr>
                                        <td class="">1020</td>
                                        <td class="">1270</td>
                                        <td class="">1100</td>
                                        <td class="">1360</td>
                                        <td class="">1660</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tableArrow tableNext">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="property__content">
                        <div class="wrap-table flex-js-fw">
                            <div class="tableArrow tablePrev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </div>
                            <table class="table1 tableName">
                                <tr>
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Объем бака, л</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Монтаж
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Тип корпуса
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Производительность, л/мин</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Внутреннее покрытие бака
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Магниевый анод
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Способ нагрева
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Конструкция
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                </tr>
                                <tr>
                                    <td class="bold">ТЭН</td>
                                </tr>

                                <tr>
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр рециркуляционного патрубка, дюйм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Максимальная рабочая температура, °С</td>
                                </tr>
                                <tr>
                                    <td class="bold">Подводка</td>
                                </tr>
                                <tr>
                                    <td class="bold">Страна производитель</td>
                                </tr>
                                <tr>
                                    <td class="bold">Тип водонагревателя</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цвет</td>
                                </tr>
                                <tr>
                                    <td class="bold">Вес без упаковки, кг</td>
                                </tr>
                                <tr>
                                    <td class="bold">Высота, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Глубина, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Ширина, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Гарантия, лет</td>
                                </tr>
                            </table>
                            <div class="tableProperty-wrap">
                                <table class="table2 tableProperty">
                                    <tr>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-img">
                                                    <img src="/assets/img/img2.png" title="" alt="">
                                                </div>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">100</td>
                                        <td class="">140</td>
                                        <td class="">200</td>
                                        <td class="">300</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                    </tr>
                                    <tr>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                    </tr>
                                    <tr>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                    </tr>
                                    <tr>
                                        <td class="">8,5</td>
                                        <td class="">12,3</td>
                                        <td class="">17,8</td>
                                        <td class="">17,8</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                    </tr>
                                    <tr>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                    </tr>
                                    <tr>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                    </tr>
                                    <tr>
                                        <td class="">21,1</td>
                                        <td class="">30,4</td>
                                        <td class="">44,8</td>
                                        <td class="">44,8</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                    </tr>
                                    <tr>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                    </tr>
                                    <tr>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                    </tr>

                                    <tr>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <div class="card">
                                                <a href="#" class="absLink"></a>
                                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                            </div>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                    </tr>
                                    <tr>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                    </tr>
                                    <tr>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                    </tr>
                                    <tr>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                    </tr>
                                    <tr>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                    </tr>
                                    <tr>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                    </tr>
                                    <tr>
                                        <td class="">60</td>
                                        <td class="">65</td>
                                        <td class="">84</td>
                                        <td class="">122</td>
                                        <td class="">147</td>
                                        <td class="">60</td>
                                        <td class="">60</td>
                                    </tr>
                                    <tr>
                                        <td class="">1020</td>
                                        <td class="">1270</td>
                                        <td class="">1100</td>
                                        <td class="">1360</td>
                                        <td class="">1660</td>
                                        <td class="">1660</td>
                                        <td class="">1660</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tableArrow tableNext">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="propertyAccord pt-bt">
                    <div class="compareProducts">
                        <div class="compareProducts-box">
                            <div class="card">
                                <div class="card-decimal">3 / 3</div>
                                <a href="#" class="absLink"></a>
                                <div class="card-img">
                                    <img src="/assets/img/img2.png" title="" alt="">
                                </div>
                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                <div class="card-btm">
                                    <a href="#" class="btn"><i class="fa fa-balance-scale fa-stack" aria-hidden="true"></i></a>
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="compareProducts-box">
                            <div class="card">
                                <div class="card-decimal">3 / 3</div>
                                <a href="#" class="absLink"></a>
                                <div class="card-img">
                                    <img src="/assets/img/img2.png" title="" alt="">
                                </div>
                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                <div class="card-btm">
                                    <a href="#" class="btn"><i class="fa fa-balance-scale fa-stack" aria-hidden="true"></i></a>
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="compareProducts-box">
                            <div class="card">
                                <div class="card-decimal">3 / 3</div>
                                <a href="#" class="absLink"></a>
                                <div class="card-img">
                                    <img src="/assets/img/img2.png" title="" alt="">
                                </div>
                                <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                <div class="card-btm">
                                    <a href="#" class="btn"><i class="fa fa-balance-scale fa-stack" aria-hidden="true"></i></a>
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="compareProducts-checkbox">
                        <div class="checkbox">
                            <input type="checkbox" id="different" name="different">
                            <label for="different">
                                <span class="text">Только различающиеся</span>
                                <span class="before"></span>
                            </label>
                        </div>
                    </div>
                    <div class="compareProducts-title bold">Цена</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box bold">23 600 р.</div>
                        <div class="compareProducts-box bold">23 600 р.</div>
                        <div class="compareProducts-box bold">23 600 р.</div>
                    </div>
                    <div class="compareProducts-title">Объем бака, л</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">80</div>
                        <div class="compareProducts-box">120</div>
                        <div class="compareProducts-box">100</div>
                    </div>
                    <div class="compareProducts-title">Монтаж</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">настенный</div>
                        <div class="compareProducts-box">настенный</div>
                        <div class="compareProducts-box">настенный</div>
                    </div>
                    <div class="compareProducts-title">Тип корпуса</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">вертикальный</div>
                        <div class="compareProducts-box">вертикальный</div>
                        <div class="compareProducts-box">вертикальный</div>
                    </div>
                    <div class="compareProducts-title bold">Внутреннее покрытие бака</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">эмаль.сталь</div>
                        <div class="compareProducts-box">эмаль.сталь</div>
                        <div class="compareProducts-box">эмаль.сталь</div>
                    </div>
                    <div class="compareProducts-title">Магниевый анод</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">есть</div>
                        <div class="compareProducts-box">есть</div>
                        <div class="compareProducts-box">есть</div>
                    </div>
                    <div class="compareProducts-title">Мощность тепловая, кВт</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">24</div>
                        <div class="compareProducts-box">24</div>
                        <div class="compareProducts-box">24</div>
                    </div>
                    <div class="compareProducts-title">Способ нагрева</div>
                    <div class="compareProducts-text">
                        <div class="compareProducts-box">комбинированый</div>
                        <div class="compareProducts-box">комбинированый</div>
                        <div class="compareProducts-box">комбинированый</div>
                    </div>
                </div>
                <div class="right pd-mb">
                    <div class="btn white">
                        Очистить список сравнения
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <?php include '../../partials/goods.php' ?>
        </div>
    </div>

<?php include '../../partials/footer.php' ?>