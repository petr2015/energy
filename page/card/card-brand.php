<?php include '../../partials/header.php' ?>

    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Бойлеры</a>
                </li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Косвенного
                        нагрева</a></li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Бойлер
                        косвенного нагрева Galmet TOWER</a></li>
                <li class="breadCrumb-li">Бойлер косвенного нагрева Galmet TOWER 100</li>
            </ul>
            <div class="wrap-maxCard card-brand">
                <h3 class="bold">Бойлер косвенного нагрева Galmet TOWER 100</h3>
                <div class="maxCard flex-js-as-fw">
                    <div class="flex-js-as-fw fancyBox-lef">
                        <div class="wrap-fancyBox flex-js-as-fw">
                            <div class="fancyBox-min">
                                <a href="/assets/img/1.png" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/1.png" class="photo">
                                </a>
                                <a href="/assets/img/2.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/2.jpg" class="photo">
                                </a>
                                <a href="/assets/img/3.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/3.jpg" class="photo">
                                </a>
                                <a href="/assets/img/4.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/4.jpg" class="photo">
                                </a>
                                <a href="/assets/img/5.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/5.jpg" class="photo">
                                </a>
                                <a href="/assets/img/6.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/6.jpg" class="photo">
                                </a>
                                <a href="/assets/img/7.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/7.jpg" class="photo">
                                </a>
                                <a href="/assets/img/8.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/8.jpg" class="photo">
                                </a>
                            </div>
                            <div class="fancyBox-max">
                                <a href="/assets/img/1.png" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/1.png">
                                </a>
                            </div>
                        </div>
                        <div class="wrap-fbText">
                            <div class="fbText"><span class="fa fa-dropbox fa-fw"></span>Доставка по всей России</div>
                            <div class="fbText"><span class="fa fa-money fa-fw"></span>Наличный и безналичный расчет
                            </div>
                        </div>
                    </div>
                    <div class="maxCard-info">
                        <div class="maxCard-minInf flex-jb-ac-fw">
                            <div class="productMin">
                                <div class="box">Код товара: <span class="bold">1358</span></div>
                                <div class="box">Производитель: <a href="#" class="minInf-link">Galmet</a></div>
                            </div>
                            <div class="productMin-price">
                                <div class="price-text">27 399 р.</div>
                                <button class="btn green">Купить <i class="fa fa-cart-plus" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                        <div class="fastKons">
                            <div class="row">
                                <div class="col-xs-12 col-md-6 mb">
                                    <div class="h4">Быстрая консультация</div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="fastKons-form">
                                        <form action="#" method="" class="callMeForm">
                                            <div class="inp">
                                                <input type="text" class="phone" name="" placeholder="+7 (___) ___-__-__">
                                                <button class="btn green" type="submit">
                                                    <span class="fa fa-phone"></span> Перезвоните
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-jb-ac-fw maxCard-btn row">
                            <div class="col-lg-4 plNon">
                                <a href="#" class="btn bold print white">Печать <span class="fa fa-print"></span></a>
                            </div>
                            <div class="col-sm-12 col-lg-4 col-md-6">
                                <a href="#" class="btn bold white addHov">
                                    сравнить
                                    <span class="fa fa-list-ul fa-stack" aria-hidden="true"></span>
                                    <span class="addHov-text">Добавить в список сравнения</span>
                                </a>
                            </div>
                            <div class="col-sm-12 col-lg-4 col-md-6">
                                <div class="btn white bold analoguesBtn">Посмотреть аналоги <i
                                            class="fa fa-balance-scale" aria-hidden="true"></i></div>
                            </div>
                        </div>

                        <div class="slider-compare">
                            <div class="owl-carousel owl-compare">
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="maxCard-text">
                            <p>Бойлер с нагревом комбинированный, объемом 100 литров. Монтаж напольный. Внутреннее
                                покрытие бака - эмаль. Производительность до 8.5 л/мин. Конструкция теплообменника -
                                змеевик. ТЭН - .
                                Гарантия 5.00 лет. Страна - Польша. . .
                            </p>
                        </div>
                        <div class="wrap-dashed pt-bt pb-bb ">
                            <h3 class="bold">Файлы</h3>
                            <a href="#" class="dashed" target="_blank" title="Загрузить документацию">
                                <span class="fa fa-file-text-o"></span> Буклет
                            </a>
                        </div>
                    </div>
                </div>

                <div class="propertyTab">
                    <div class="property__caption ">
                        <div class="property-link active">
                            <span class="fa fa-list-ul" aria-hidden="true"></span>
                            <span class="dotted">Технические характеристики</span>
                        </div>
                        <div class="property-link">
                            <span class="fa fa-align-justify" aria-hidden="true"></span>
                            <span class="dotted">Описание</span>
                        </div>
                        <div class="property-link lgNon">
                            <span class="fa fa-list-ul" aria-hidden="true"></span>
                            <span class="dotted">Аналоги</span>
                        </div>
                        <div class="property-link">
                            <span class="fa fa-list-ul" aria-hidden="true"></span>
                            <span class="dotted">Запчасти</span>
                        </div>
                    </div>
                    <div class="property__content active">
                        <h3 class="bold">Технические характеристики Galmet TOWER 100</h3>
                        <div class="table-hover">
                            <table class="table table-striped table-bordered" cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            Объем бака, л
                                        </div>
                                    </td>
                                    <td>100</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Монтаж
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>напольный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Тип корпуса
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>вертикальный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Производительность, л/мин
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>8,5</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Внутреннее покрытие бака
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>эмаль</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Магниевый анод
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>есть</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>21,1</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Способ нагрева
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>комбинированный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Конструкция
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>змеевик</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>20 [3/4"]</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Диаметр трубы отопления, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>20 [3/4"]</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            ТЭН
                                        </div>
                                    </td>
                                    <td>опция</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="center">
                            <div class="btn btnTab active">
                                <i class="fa fa-arrow-down"></i>
                                Все характеристики
                                <i class="fa fa-arrow-down"></i>
                            </div>
                            <div class="btn btnTab">
                                <i class="fa fa-arrow-up"></i>
                                Скрыть характеристики
                                <i class="fa fa-arrow-up"></i>
                            </div>
                        </div>
                    </div>
                    <div class="property__content">
                        <h3 class="bold">Описание Galmet TOWER 100</h3>
                        <div class="prod-desc">
                            <h3>Особенности</h3>
                            <ul>
                                <li>толщина стали рабочего бака 2,5-5 мм;</li>
                                <li>возможность установки электрической части 2-3 кВт в резьбовое отверстие 5/4";</li>
                                <li>мощный теплообменник;</li>
                                <li>огромный защитный магниевый анод;</li>
                                <li>предусмотрен циркуляционный патрубок;</li>
                                <li>возможность применения внешнего термостата.</li>
                            </ul>
                            <h3>Преимущества</h3>
                            <ul>
                                <li>наиболее дешевый способ приготовления необходимого количества горячей воды;</li>
                                <li>быстрый нагрев воды за счет большой площади теплообменника;</li>
                                <li>возможность использования в летний период благодаря электрическому нагреву
                                    (опция);
                                </li>
                                <li>простота обслуживания</li>
                            </ul>
                            <div>
                                <p>Номинальный объем л 100</p>
                                <p>Мах. рабочее давление рабочего бака MПa 0,6</p>
                                <p>Мах. рабочее давление теплообменника MПa 0,6</p>
                                <p>Площадь теплообменника м2 0.6</p>
                                <p>Мощность теплообменника (70/10/45°C) кВт 16</p>
                                <p>Производительность л/ч 390</p>
                                <p>Мощность теплообменника (80/10/45°C) кВт 21,1</p>
                                <p>Производительность л/ч 510</p>
                                <p>Потребность теплоносителя м3/ч 2.5</p>
                                <p>Магниевый анод мм 25x390</p>
                                <p>h1 - Подача холодной воды мм 210</p>
                                <p>h2 - Возврат теплоносителя мм 310</p>
                                <p>h3 - Гильза датчика термостата мм 400</p>
                                <p>h4 - Циркуляция мм 500</p>
                                <p>h5 - Подача теплоносителя мм 710</p>
                                <p>h6 - Отбор горячей воды мм 790</p>
                                <p>L мм 1020</p>
                                <p>D мм 518</p>
                                <p>Вес нетто кг 60</p>
                            </div>
                        </div>
                    </div>
                    <div class="property__content lgNon">

                        <div class=" pT pt-bt">
                            <div class=" p_cn">
                                <div class=" p_l active">
                                    <span class="fa fa-balance-scale" aria-hidden="true"></span>
                                    <span class="dotted">Различающиеся</span>
                                </div>
                                <div class=" p_l">
                                    <span class="fa fa-list" aria-hidden="true"></span>
                                    <span class="dotted">Все характеристики</span>
                                </div>
                            </div>
                            <div class=" p_ct active">
                                <div class="wrap-table flex-js-fw">
                                    <div class="tableArrow tablePrev">
                                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                                    </div>
                                    <table class="tableName table1">
                                        <tbody>
                                        <tr data-height="1" style="height: 261px;">
                                            <td class="bold">Технические характеристики TOWER</td>
                                        </tr>
                                        <tr style="height: 44px;">
                                            <td class="bold">Цена</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">Объем бака, л</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Производительность, л/мин
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Мощность тепловая, кВт
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Диаметр трубы ГВС, мм (дюйм)
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">Вес без упаковки, кг</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">Высота, мм</td>
                                        </tr>
                                        <tr>
                                            <td class="bold">Глубина, мм</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="tableProperty-wrap">
                                        <table class="table2 tableProperty">
                                            <tbody>
                                            <tr>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bold">
                                                    27 300 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    31 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    38 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    45 800 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">100</td>
                                                <td class="">140</td>
                                                <td class="">200</td>
                                                <td class="">300</td>
                                                <td class="">400</td>
                                                <td class="">400</td>
                                                <td class="">400</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">8,5</td>
                                                <td class="">12,3</td>
                                                <td class="">17,8</td>
                                                <td class="">17,8</td>
                                                <td class="">23</td>
                                                <td class="">23</td>
                                                <td class="">23</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">21,1</td>
                                                <td class="">30,4</td>
                                                <td class="">44,8</td>
                                                <td class="">44,8</td>
                                                <td class="">57,6</td>
                                                <td class="">57,6</td>
                                                <td class="">57,6</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">20 [3/4"]</td>
                                                <td class="">20 [3/4"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                            </tr>
                                            <tr style="height: 47px;">
                                                <td class="">60</td>
                                                <td class="">65</td>
                                                <td class="">84</td>
                                                <td class="">122</td>
                                                <td class="">147</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">1020</td>
                                                <td class="">1270</td>
                                                <td class="">1100</td>
                                                <td class="">1360</td>
                                                <td class="">1660</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">518</td>
                                                <td class="">518</td>
                                                <td class="">670</td>
                                                <td class="">670</td>
                                                <td class="">700</td>
                                            </tr>
                                            <tr style="height: 29px;">
                                                <td class="">518</td>
                                                <td class="">518</td>
                                                <td class="">670</td>
                                                <td class="">670</td>
                                                <td class="">700</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tableArrow tableNext">
                                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class=" p_ct">
                                <div class="wrap-table flex-js-fw">
                                    <div class="tableArrow tablePrev">
                                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                                    </div>
                                    <table class="table1 tableName">
                                        <tbody>
                                        <tr style="">
                                            <td class="bold">Технические характеристики TOWER</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Цена</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Объем бака, л</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Монтаж
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Тип корпуса
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Производительность, л/мин</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Внутреннее покрытие бака
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Магниевый анод
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Мощность тепловая, кВт
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Способ нагрева
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Конструкция
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">
                                                <div class="wrap-tip">
                                                    Диаметр трубы ГВС, мм (дюйм)
                                                    <div class="tip">
                                                        <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                        <div class="tip-hov">
                                                            <div class="tip-tit">Подводка</div>
                                                            <div class="tip-text">Сторона прибора с которой производятся
                                                                подключение воды/ электричества/ газа
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">ТЭН</td>
                                        </tr>

                                        <tr style="">
                                            <td class="bold">Технические характеристики TOWER</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Цена</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Диаметр рециркуляционного патрубка, дюйм</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Максимальная рабочая температура, °С</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Подводка</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Страна производитель</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Тип водонагревателя</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Цвет</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Вес без упаковки, кг</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Высота, мм</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Глубина, мм</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Ширина, мм</td>
                                        </tr>
                                        <tr style="">
                                            <td class="bold">Гарантия, лет</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="tableProperty-wrap">
                                        <table class="table2 tableProperty">
                                            <tbody>
                                            <tr>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-img">
                                                            <img src="/assets/img/img2.png" title="" alt="">
                                                        </div>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bold">
                                                    27 300 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    31 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    38 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    45 800 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">100</td>
                                                <td class="">140</td>
                                                <td class="">200</td>
                                                <td class="">300</td>
                                                <td class="">400</td>
                                                <td class="">400</td>
                                                <td class="">400</td>
                                            </tr>
                                            <tr>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                                <td class="">напольный</td>
                                            </tr>
                                            <tr>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                                <td class="">вертикальный</td>
                                            </tr>
                                            <tr>
                                                <td class="">8,5</td>
                                                <td class="">12,3</td>
                                                <td class="">17,8</td>
                                                <td class="">17,8</td>
                                                <td class="">23</td>
                                                <td class="">23</td>
                                                <td class="">23</td>
                                            </tr>
                                            <tr>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                                <td class="">эмаль</td>
                                            </tr>
                                            <tr>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                                <td class="">есть</td>
                                            </tr>
                                            <tr>
                                                <td class="">21,1</td>
                                                <td class="">30,4</td>
                                                <td class="">44,8</td>
                                                <td class="">44,8</td>
                                                <td class="">57,6</td>
                                                <td class="">57,6</td>
                                                <td class="">57,6</td>
                                            </tr>
                                            <tr>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                                <td class="">комбинированный</td>
                                            </tr>
                                            <tr>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                                <td class="">змеевик</td>
                                            </tr>
                                            <tr>
                                                <td class="">20 [3/4"]</td>
                                                <td class="">20 [3/4"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                            </tr>
                                            <tr>
                                                <td class="">20 [3/4"]</td>
                                                <td class="">20 [3/4"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                                <td class="">25 [1"]</td>
                                            </tr>
                                            <tr>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                                <td class="">опция</td>
                                            </tr>

                                            <tr>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                                <td class="name">
                                                    <div class="card">
                                                        <a href="#" class="absLink"></a>
                                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER
                                                        </div>
                                                    </div>
                                                    <div class="detailsTd">
                                                        <a href="#" class="btn white bold">Подробнее</a>
                                                        <a href="#" class="btn white">
                                                            <i class="fa fa-list-ul fa-stack" aria-hidden="true"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="bold">
                                                    27 300 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    31 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    38 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    45 800 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                                <td class="bold">
                                                    61 000 р.
                                                    <button type="submit" class="btn green">
                                                        <span class="fa fa-shopping-cart fa-fw"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                                <td class="">3/4</td>
                                            </tr>
                                            <tr>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                                <td class="">80</td>
                                            </tr>
                                            <tr>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                                <td class="">боковая</td>
                                            </tr>
                                            <tr>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                                <td class="">Польша</td>
                                            </tr>
                                            <tr>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                                <td class="">накопительный</td>
                                            </tr>
                                            <tr>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                                <td class="">белый</td>
                                            </tr>
                                            <tr>
                                                <td class="">60</td>
                                                <td class="">65</td>
                                                <td class="">84</td>
                                                <td class="">122</td>
                                                <td class="">147</td>
                                                <td class="">60</td>
                                                <td class="">60</td>
                                            </tr>
                                            <tr>
                                                <td class="">1020</td>
                                                <td class="">1270</td>
                                                <td class="">1100</td>
                                                <td class="">1360</td>
                                                <td class="">1660</td>
                                                <td class="">1660</td>
                                                <td class="">1660</td>
                                            </tr>
                                            <tr>
                                                <td class="">518</td>
                                                <td class="">518</td>
                                                <td class="">670</td>
                                                <td class="">670</td>
                                                <td class="">700</td>
                                                <td class="">700</td>
                                                <td class="">700</td>
                                            </tr>
                                            <tr>
                                                <td class="">518</td>
                                                <td class="">518</td>
                                                <td class="">670</td>
                                                <td class="">670</td>
                                                <td class="">700</td>
                                                <td class="">700</td>
                                                <td class="">700</td>
                                            </tr>
                                            <tr>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                                <td class="">5</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tableArrow tableNext">
                                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="property__content flex-fw">
                        <div class="col-xs-12 col-sm-6 col-pd5">
                            <div class="spareParts">
                                <a href="#" class="spareParts-img spareParts-box">
                                    <img src="/assets/img/galmet.jpg" title="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''"
                                             alt="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''">
                                </a>
                                <div class="spareParts-text spareParts-box">
                                    <p>запчасть</p>
                                    <a class="" href="#">Galmet Электрический ТЭН 3 kW 230 V - K5/4''</a>
                                </div>
                                <div class="spareParts-price spareParts-box">
                                    <span>+ 5 300 р.</span>
                                </div>
                                <div class="spareParts-btn spareParts-box">
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-pd5">
                            <div class="spareParts">
                                <a href="#" class="spareParts-img spareParts-box">
                                    <img src="/assets/img/galmet.jpg" title="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''"
                                             alt="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''">
                                </a>
                                <div class="spareParts-text spareParts-box">
                                    <p>запчасть</p>
                                    <a class="" href="#">Galmet Электрический ТЭН 3 kW 230 V - K5/4''</a>
                                </div>
                                <div class="spareParts-price spareParts-box">
                                    <span>+ 5 300 р.</span>
                                </div>
                                <div class="spareParts-btn spareParts-box">
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-pd5">
                            <div class="spareParts">
                                <a href="#" class="spareParts-img spareParts-box">
                                    <img src="/assets/img/galmet.jpg" title="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''"
                                         alt="Запчасть Galmet Электрический ТЭН 3 kW 230 V - K5/4''">
                                </a>
                                <div class="spareParts-text spareParts-box">
                                    <p>запчасть</p>
                                    <a class="" href="#">Galmet Электрический ТЭН 3 kW 230 V - K5/4''</a>
                                </div>
                                <div class="spareParts-price spareParts-box">
                                    <span>+ 5 300 р.</span>
                                </div>
                                <div class="spareParts-btn spareParts-box">
                                    <button class="btn green"><i class="fa fa-shopping-cart fa-fw" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mb4">
                    <p>Интернет-магазин Энергомир предлагает широкий выбор горелочного оборудования, котлов и агрегатов
                        водяного и воздушного отопления, водонагревателей, насосов и другого климатического
                        оборудования. <a href="#">Выбрать радиаторы</a> по низким ценам.
                        Приобретенный товар доставляется по всей России.</p>
                    <p>Если бойлер косвенного нагрева Galmet TOWER 100 есть в наличии, мы доставим его за 1 рабочий
                        день. Также вы можете заказать доставку на выбранную дату.</p>
                </div>

            </div>
            <?php include '../../partials/goods.php' ?>
        </div>
    </div>

<?php include '../../partials/footer.php' ?>