<?php include '../../partials/header.php' ?>
    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Бойлеры</a>
                </li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Косвенного
                        нагрева</a></li>
                <li class="breadCrumb-li">Бойлер косвенного нагрева Galmet TOWER</li>
            </ul>
            <div class="wrap-maxCard">
                <h3 class="bold">Бойлер косвенного нагрева Galmet TOWER (серия)</h3>
                <div class="maxCard flex-js-as-fw">
                    <div class="flex-js-as-fw fancyBox-lef">
                        <div class="wrap-fancyBox flex-js-as-fw">
                            <div class="fancyBox-min">
                                <a href="/assets/img/1.png" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/1.png" class="photo">
                                </a>
                                <a href="/assets/img/2.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/2.jpg" class="photo">
                                </a>
                                <a href="/assets/img/3.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/3.jpg" class="photo">
                                </a>
                                <a href="/assets/img/4.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/4.jpg" class="photo">
                                </a>
                                <a href="/assets/img/5.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/5.jpg" class="photo">
                                </a>
                                <a href="/assets/img/6.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/6.jpg" class="photo">
                                </a>
                                <a href="/assets/img/7.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/7.jpg" class="photo">
                                </a>
                                <a href="/assets/img/8.jpg" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/8.jpg" class="photo">
                                </a>
                            </div>
                            <div class="fancyBox-max">
                                <a href="/assets/img/1.png" class="fancybox" rel="gallery" title="текст">
                                    <img src="/assets/img/1.png">
                                </a>
                            </div>
                        </div>
                        <div class="wrap-fbText">
                            <div class="fbText"><span class="fa fa-dropbox fa-fw"></span>Доставка по всей России</div>
                            <div class="fbText"><span class="fa fa-money fa-fw"></span>Наличный и безналичный расчет
                            </div>
                        </div>
                        <div class="fastKons">
                            <div class="h4">Быстрая консультация</div>
                            <div class="fastKons-form">
                                <form action="#" method="" class="callMeForm">
                                    <div class="inp">
                                        <input type="text" class="phone" name="" placeholder="+7 (___) ___-__-__">
                                        <button class="btn green" type="submit">
                                            <span class="fa fa-phone"></span> Перезвоните
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="wrap-dashed">
                            <h3 class="bold">Файлы</h3>
                            <a href="#" class="dashed" target="_blank" title="Загрузить документацию">
                                <span class="fa fa-file-text-o"></span> Буклет
                            </a>
                        </div>
                    </div>
                    <div class="maxCard-info">
                        <div class="maxCard-minInf flex-jb-ac-fw">
                            <div class="productMin">
                                <div class="box">Код товара: <span class="bold">1358</span></div>
                                <div class="box">Производитель: <a href="#" class="minInf-link">Galmet</a></div>
                            </div>
                            <a href="#" class="print"><span class="fa fa-print"></span>Версия для печати</a>
                        </div>
                        <div class="maxCard-text">
                            <p>Максимально быстрый нагрев за счет мощного спиралевидного теплообменника с большой
                                площадью теплообмена
                                <br>
                                Возможность ускоренного нагрева, а также использование в летний период времени за счет
                                установки электрического ТЭНа.
                                <br>
                                Минимальные теплопотери благодаря увеличению слоя теплоизоляции из пенополиуретана.
                                <br>
                                Увеличенный срок службы - от 10 лет благодаря покрытию бака титан-кобальтовой эмалью,
                                применению технологии «сухого» ТЭНа и использованию двух больших магниевых анодов.
                                <br>
                                Гарантия 60 месяцев от официального представительства Galmet на территории РФ.
                            </p>
                        </div>
                        <div class="h3">ВНИМАНИЕ! НА ДАННОЕ ОБОРУДОВАНИЕ ВОЗМОЖНЫ КРЕДИТ И РАССРОЧКА!</div>
                        <div class="wrap-slider">
                            <div class="model">МОДЕЛИ GALMET TOWER (<span>9</span>)</div>
                            <div class="owl-carousel owl-model">
                                <div class="item">
                                    <div class="owlModel">
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="owlModel">
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="owlModel">
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="owlModel">
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                        <div class="owlModel-card flex-js-ac-fw">
                                            <div class="modelCard-img">
                                                <img src="/assets/img/1.png" title="" alt="">
                                            </div>
                                            <div class="modelCard-rig">
                                                <div class="modelCard-text">Бойлер косвенного нагрева</div>
                                                <a href="#" class="modelCard-link fs12">Galmet TOWER 100</a>
                                                <div class="modelCard-text">27 300 р.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-je-ac-fw maxCard-btn">
                            <a href="#compare" class="btn green plNon">Сравнить все модели</a>
                            <div class="btn analoguesBtn">Посмотреть аналоги <i class="fa fa-balance-scale" aria-hidden="true"></i></div>
                        </div>
                        <div class="slider-compare">
                            <div class="owl-carousel owl-compare">
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Низкая цена</div>
                                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                                        <div class="card-price bold">От 27 300 р.</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <h3 id="compare" class="bold">Цены и характеристики моделей Galmet TOWER</h3>
                <div class="propertyTab">
                    <div class="property__caption">
                        <div class="property-link active">
                            <span class="fa fa-list-ul" aria-hidden="true"></span>
                            <span class="dotted">Основные</span>
                        </div>
                        <div class="property-link">
                            <span class="fa fa-balance-scale" aria-hidden="true"></span>
                            <span class="dotted">Различающиеся</span>
                        </div>
                        <div class="property-link allTab ">
                            <span class="fa fa-list" aria-hidden="true"></span>
                            <span class="dotted">Все характеристики</span>
                        </div>
                    </div>
                    <div class="property__content active">
                        <div class="wrap-table flex-js-fw">
                            <div class="tableArrow tablePrev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </div>
                            <table class="tableName table1">
                                <tr>
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Объем бака, л</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Монтаж
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Тип корпуса
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Производительность, л/мин
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Внутреннее покрытие бака
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Магниевый анод
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Способ нагрева
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Конструкция
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                </tr>
                                <tr>
                                    <td class="bold">ТЭН</td>
                                </tr>
                            </table>
                            <div class="tableProperty-wrap">
                                <table class="tableProperty table2">
                                    <tr>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 100</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 140</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 200</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 300</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white addHov">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                    <span class="addHov-text">Добавить в список сравнения</span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">100</td>
                                        <td class="">140</td>
                                        <td class="">200</td>
                                        <td class="">300</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                    </tr>
                                    <tr>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                    </tr>
                                    <tr>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                    </tr>
                                    <tr>
                                        <td class="">8,5</td>
                                        <td class="">12,3</td>
                                        <td class="">17,8</td>
                                        <td class="">17,8</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                    </tr>
                                    <tr>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                    </tr>
                                    <tr>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                    </tr>
                                    <tr>
                                        <td class="">21,1</td>
                                        <td class="">30,4</td>
                                        <td class="">44,8</td>
                                        <td class="">44,8</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                    </tr>
                                    <tr>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                    </tr>
                                    <tr>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tableArrow tableNext">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="property__content">
                        <div class="wrap-table flex-js-fw">
                            <div class="tableArrow tablePrev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </div>
                            <table class="tableName table1">
                                <tr data-height="1">
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Объем бака, л</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Производительность, л/мин
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                </tr>
                                <tr>
                                    <td class="bold">Вес без упаковки, кг</td>
                                </tr>
                                <tr>
                                    <td class="bold">Высота, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Глубина, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Ширина, мм</td>
                                </tr>
                            </table>
                            <div class="tableProperty-wrap">
                                <table class="table2 tableProperty">
                                    <tr>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 100</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 140</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 200</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 300</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">100</td>
                                        <td class="">140</td>
                                        <td class="">200</td>
                                        <td class="">300</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                    </tr>
                                    <tr>
                                        <td class="">8,5</td>
                                        <td class="">12,3</td>
                                        <td class="">17,8</td>
                                        <td class="">17,8</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                    </tr>
                                    <tr>
                                        <td class="">21,1</td>
                                        <td class="">30,4</td>
                                        <td class="">44,8</td>
                                        <td class="">44,8</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">60</td>
                                        <td class="">65</td>
                                        <td class="">84</td>
                                        <td class="">122</td>
                                        <td class="">147</td>
                                    </tr>
                                    <tr>
                                        <td class="">1020</td>
                                        <td class="">1270</td>
                                        <td class="">1100</td>
                                        <td class="">1360</td>
                                        <td class="">1660</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tableArrow tableNext">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="property__content allTab">
                        <div class="wrap-table flex-js-fw">
                            <div class="tableArrow tablePrev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                            </div>
                            <table class="table1 tableName">
                                <tr data-height="1">
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Объем бака, л</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Монтаж
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Тип корпуса
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Производительность, л/мин</td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Внутреннее покрытие бака
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Магниевый анод
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Мощность тепловая, кВт
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Способ нагрева
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Конструкция
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">
                                        <div class="wrap-tip">
                                            Диаметр трубы ГВС, мм (дюйм)
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр трубы отопления, мм (дюйм)</td>
                                </tr>
                                <tr>
                                    <td class="bold">ТЭН</td>
                                </tr>

                                <tr>
                                    <td class="bold">Технические характеристики TOWER</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цена</td>
                                </tr>
                                <tr>
                                    <td class="bold">Диаметр рециркуляционного патрубка, дюйм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Максимальная рабочая температура, °С</td>
                                </tr>
                                <tr>
                                    <td class="bold">Подводка</td>
                                </tr>
                                <tr>
                                    <td class="bold">Страна производитель</td>
                                </tr>
                                <tr>
                                    <td class="bold">Тип водонагревателя</td>
                                </tr>
                                <tr>
                                    <td class="bold">Цвет</td>
                                </tr>
                                <tr>
                                    <td class="bold">Вес без упаковки, кг</td>
                                </tr>
                                <tr>
                                    <td class="bold">Высота, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Глубина, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Ширина, мм</td>
                                </tr>
                                <tr>
                                    <td class="bold">Гарантия, лет</td>
                                </tr>
                            </table>
                            <div class="tableProperty-wrap">
                                <table class="table2 tableProperty">
                                    <tr>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 100</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 140</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 200</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 300</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">100</td>
                                        <td class="">140</td>
                                        <td class="">200</td>
                                        <td class="">300</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                        <td class="">400</td>
                                    </tr>
                                    <tr>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                        <td class="">напольный</td>
                                    </tr>
                                    <tr>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                        <td class="">вертикальный</td>
                                    </tr>
                                    <tr>
                                        <td class="">8,5</td>
                                        <td class="">12,3</td>
                                        <td class="">17,8</td>
                                        <td class="">17,8</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                        <td class="">23</td>
                                    </tr>
                                    <tr>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                        <td class="">эмаль</td>
                                    </tr>
                                    <tr>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                        <td class="">есть</td>
                                    </tr>
                                    <tr>
                                        <td class="">21,1</td>
                                        <td class="">30,4</td>
                                        <td class="">44,8</td>
                                        <td class="">44,8</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                        <td class="">57,6</td>
                                    </tr>
                                    <tr>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                        <td class="">комбинированный</td>
                                    </tr>
                                    <tr>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                        <td class="">змеевик</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">20 [3/4"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                        <td class="">25 [1"]</td>
                                    </tr>
                                    <tr>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                        <td class="">опция</td>
                                    </tr>

                                    <tr>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 100</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 140</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 200</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 300</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="name">
                                            <a href="#" class="name-link bold">TOWER 400</a>
                                            <div class="detailsTd">
                                                <a href="#" class="btn white bold">Подробнее</a>
                                                <a href="#" class="btn white">
                                                    <span class="fa fa-balance-scale fa-stack"
                                                          aria-hidden="true"></span>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="bold">
                                            27 300 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            31 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            38 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            45 800 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                        <td class="bold">
                                            61 000 р.
                                            <button type="submit" class="btn green">
                                                <span class="fa fa-shopping-cart fa-fw"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                        <td class="">3/4</td>
                                    </tr>
                                    <tr>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                        <td class="">80</td>
                                    </tr>
                                    <tr>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                        <td class="">боковая</td>
                                    </tr>
                                    <tr>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                        <td class="">Польша</td>
                                    </tr>
                                    <tr>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                        <td class="">накопительный</td>
                                    </tr>
                                    <tr>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                        <td class="">белый</td>
                                    </tr>
                                    <tr>
                                        <td class="">60</td>
                                        <td class="">65</td>
                                        <td class="">84</td>
                                        <td class="">122</td>
                                        <td class="">147</td>
                                        <td class="">60</td>
                                        <td class="">60</td>
                                    </tr>
                                    <tr>
                                        <td class="">1020</td>
                                        <td class="">1270</td>
                                        <td class="">1100</td>
                                        <td class="">1360</td>
                                        <td class="">1660</td>
                                        <td class="">1660</td>
                                        <td class="">1660</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">518</td>
                                        <td class="">518</td>
                                        <td class="">670</td>
                                        <td class="">670</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                        <td class="">700</td>
                                    </tr>
                                    <tr>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                        <td class="">5</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="tableArrow tableNext">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                    <div class="center">
                        <div class="btn btnTab active">
                            <i class="fa fa-arrow-down"></i>
                            Все характеристики
                            <i class="fa fa-arrow-down"></i>
                        </div>
                        <div class="btn btnTab">
                            <i class="fa fa-arrow-up"></i>
                            Скрыть характеристики
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </div>
                </div>
                <div class="propertyAccord">
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart"
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart"
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart"
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart"
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart "
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                    <div class="wrap-parameterMob">
                        <div class="parameterMob">
                            <div class="parameterMob-name">TOWER 100</div>
                            <div class="parameterMob-price">27 300 р.</div>
                        </div>
                        <div class="parameterMob-open">
                            <div class="parameterMob-open-btn row">
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a rel="nofollow" class="btn red" href="#">Подробнее <i class="fa fa-angle-right"
                                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn green">Купить <i class="fa fa-shopping-cart"
                                                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-mb-10">
                                    <a href="#" class="btn white">Сравнить <i class="fa fa-balance-scale fa-stack"
                                                                              aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="parameterMob-open-table">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <span class="bold">Объем бака, л</span>
                                        </td>
                                        <td>
                                            100
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Монтаж
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            напольный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Тип корпуса
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            вертикальный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Производительность, л/мин
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            8,5
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Внутреннее покрытие бака
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            эмаль
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Магниевый анод
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            есть
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Мощность тепловая, кВт
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            21,1
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Способ нагрева
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                        <td>
                                            комбинированный
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="bold wrap-tip">
                                                Конструкция
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            змеевик
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="wrap-tip bold">
                                                Диаметр трубы ГВС, мм (дюйм)
                                                <div class="tip">
                                                    <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                    <div class="tip-hov">
                                                        <div class="tip-tit">Подводка</div>
                                                        <div class="tip-text">Сторона прибора с которой производятся
                                                            подключение воды/ электричества/ газа
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">Диаметр трубы отопления, мм (дюйм)</span>
                                        </td>
                                        <td>
                                            20 [3/4"]
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="bold">ТЭН</span>
                                        </td>
                                        <td>
                                            опция
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-12">
                                <button class="more-param">Показать все</button>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="description">
                    <h3 class="bold mobClick">Описание</h3>
                    <div class="description-inf">
                        <p>Бойлер косвенного нагрева с одним спиральным теплообменником напольного монтажа вертикального
                            расположения с функцией установки электрического ТЭНа
                        </p>
                        <h3>Особенности</h3>
                        <ul>
                            <li>толщина стали рабочего бака 2,5-5 мм;</li>
                            <li>возможность установки двух ТЭНов;</li>
                            <li>мощный теплообменник;</li>
                            <li>огромный защитный магниевый анод;</li>
                            <li>предусмотрен циркуляционный патрубок;</li>
                            <li>термометр;</li>
                            <li>мягкая обшивка из эко кожи;</li>
                            <li>регулируемые ножки по высоте;</li>
                            <li>предохранительный клапан;</li>
                            <li>возможность применения внешнего термостата.</li>
                        </ul>
                        <h3>Преимущества</h3>
                        <ul>
                            <li>наиболее дешевый способ приготовления необходимого количества горячей воды;</li>
                            <li>быстрый нагрев воды за счет большой площади теплообменника;</li>
                            <li>возможность использования в летний период благодаря электрическому нагреву (опция);</li>
                            <li>простота обслуживания;</li>
                            <li>серия «BIG TOWER» в съемной теплоизоляции</li>
                        </ul>
                        <h3>Схема бойлеров Galmet Tower</h3>
                        <p><img src="/assets/img/TowerScheme.jpg" alt="Схема Galmet Tower" title="Схема Galmet Tower">
                        </p>

                        <h3>Технические характеристики</h3>
                        <div class="table-description">
                            <table border="1"
                                   class="table table-bordered table-condensed table-hover table-striped table-style">
                                <tbody>
                                <tr>
                                    <td colspan="2">Номинальный объем</td>
                                    <td>л</td>
                                    <td>100</td>
                                    <td>120</td>
                                    <td>140</td>
                                    <td>200</td>
                                    <td>300</td>
                                    <td>400</td>
                                    <td>500</td>
                                    <td>720</td>
                                    <td>1000</td>
                                    <td>1500</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Мах. рабочее давление рабочего бака</td>
                                    <td>MПa</td>
                                    <td>0,6</td>
                                    <td>0,6</td>
                                    <td>0,6</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                    <td>1.0</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Мах. рабочее давление теплообменника</td>
                                    <td>MПa</td>
                                    <td>0,6</td>
                                    <td>0,6</td>
                                    <td>0,6</td>
                                    <td>1.6</td>
                                    <td>1.6</td>
                                    <td>1.6</td>
                                    <td>1.6</td>
                                    <td>1,6</td>
                                    <td>1,6</td>
                                    <td>1,6</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Площадь теплообменника</td>
                                    <td>м2</td>
                                    <td>0.6</td>
                                    <td>0.95</td>
                                    <td>0.95</td>
                                    <td>1.4</td>
                                    <td>1.4</td>
                                    <td>1.8</td>
                                    <td>2.0</td>
                                    <td>2,4</td>
                                    <td>2,7</td>
                                    <td>2,7</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Мощность теплообменника (70/10/45°C)</td>
                                    <td>кВт</td>
                                    <td>16</td>
                                    <td>23</td>
                                    <td>23</td>
                                    <td>33,6</td>
                                    <td>33,6</td>
                                    <td>43</td>
                                    <td>48</td>
                                    <td>57,6</td>
                                    <td>64,8</td>
                                    <td>64,8</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Производительность</td>
                                    <td>л/ч</td>
                                    <td>390</td>
                                    <td>560</td>
                                    <td>560</td>
                                    <td>800</td>
                                    <td>800</td>
                                    <td>1030</td>
                                    <td>1150</td>
                                    <td>1380</td>
                                    <td>1580</td>
                                    <td>1580</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Мощность теплообменника (80/10/45°C)</td>
                                    <td>кВт</td>
                                    <td>21,1</td>
                                    <td>30,4</td>
                                    <td>30,4</td>
                                    <td>44,8</td>
                                    <td>44,8</td>
                                    <td>57,6</td>
                                    <td>64</td>
                                    <td>76,8</td>
                                    <td>86,4</td>
                                    <td>86,4</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Производительность</td>
                                    <td>л/ч</td>
                                    <td>510</td>
                                    <td>740</td>
                                    <td>740</td>
                                    <td>1070</td>
                                    <td>1070</td>
                                    <td>1380</td>
                                    <td>1530</td>
                                    <td>1840</td>
                                    <td>2110</td>
                                    <td>2110</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Потребность теплоносителя</td>
                                    <td>м3/ч</td>
                                    <td>2.5</td>
                                    <td>2.5</td>
                                    <td>2.6</td>
                                    <td>2.7</td>
                                    <td>3</td>
                                    <td>3</td>
                                    <td>3.0</td>
                                    <td>4</td>
                                    <td>4.5</td>
                                    <td>4.5</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Магниевый анод</td>
                                    <td>мм</td>
                                    <td>25x390</td>
                                    <td>25x390</td>
                                    <td>25x390</td>
                                    <td>38x400</td>
                                    <td>38x400</td>
                                    <td>38x400</td>
                                    <td>38x400</td>
                                    <td>38x600</td>
                                    <td>38x600</td>
                                    <td>38x600</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h1 - Подача холодной воды</td>
                                    <td>мм</td>
                                    <td>210</td>
                                    <td>165</td>
                                    <td>165</td>
                                    <td>210</td>
                                    <td>210</td>
                                    <td>240</td>
                                    <td>240</td>
                                    <td>350</td>
                                    <td>370</td>
                                    <td>370</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h2 - Возврат теплоносителя</td>
                                    <td>мм</td>
                                    <td>310</td>
                                    <td>250</td>
                                    <td>250</td>
                                    <td>290</td>
                                    <td>290</td>
                                    <td>320</td>
                                    <td>320</td>
                                    <td>430</td>
                                    <td>450</td>
                                    <td>450</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h3 - Гильза датчика термостата</td>
                                    <td>мм</td>
                                    <td>400</td>
                                    <td>375</td>
                                    <td>375</td>
                                    <td>435</td>
                                    <td>435</td>
                                    <td>570</td>
                                    <td>530</td>
                                    <td>650</td>
                                    <td>600</td>
                                    <td>600</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h4 - Циркуляция</td>
                                    <td>мм</td>
                                    <td>500</td>
                                    <td>450</td>
                                    <td>450</td>
                                    <td>680</td>
                                    <td>650</td>
                                    <td>770</td>
                                    <td>850</td>
                                    <td>910</td>
                                    <td>750</td>
                                    <td>750</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h5 - Подача теплоносителя</td>
                                    <td>мм</td>
                                    <td>710</td>
                                    <td>750</td>
                                    <td>750</td>
                                    <td>790</td>
                                    <td>750</td>
                                    <td>870</td>
                                    <td>970</td>
                                    <td>1030</td>
                                    <td>1000</td>
                                    <td>1000</td>
                                </tr>
                                <tr>
                                    <td colspan="2">h6 - Отбор горячей воды</td>
                                    <td>мм</td>
                                    <td>790</td>
                                    <td>920</td>
                                    <td>1070</td>
                                    <td>860</td>
                                    <td>1135</td>
                                    <td>1420</td>
                                    <td>1650</td>
                                    <td>1770</td>
                                    <td>1590</td>
                                    <td>2270</td>
                                </tr>
                                <tr>
                                    <td colspan="2">L</td>
                                    <td>мм</td>
                                    <td>1020</td>
                                    <td>1120</td>
                                    <td>1270</td>
                                    <td>1100</td>
                                    <td>1360</td>
                                    <td>1660</td>
                                    <td>1890</td>
                                    <td>2140</td>
                                    <td>1900</td>
                                    <td>2730</td>
                                </tr>
                                <tr>
                                    <td colspan="2">D</td>
                                    <td>мм</td>
                                    <td>518</td>
                                    <td>518</td>
                                    <td>518</td>
                                    <td>670</td>
                                    <td>670</td>
                                    <td>700</td>
                                    <td>700</td>
                                    <td>855/900</td>
                                    <td>1055/1100</td>
                                    <td>1055/1100</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Вес нетто</td>
                                    <td>кг</td>
                                    <td>60</td>
                                    <td>60</td>
                                    <td>65</td>
                                    <td>84</td>
                                    <td>122</td>
                                    <td>147</td>
                                    <td>195</td>
                                    <td>260</td>
                                    <td>415</td>
                                    <td>540</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="main-param">
                    <h3 class="bold mobClick">Основные параметры</h3>
                    <div class="mainParameters row">
                        <div class="col-sm-12 col-md-6 col-lg-6 mb2-mob">
                            <table class="table-mainParameters table-striped table-hover table-bordered" border="1">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Монтаж
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td style="">напольный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Внутреннее покрытие бака
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td style="">эмаль</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Магниевый анод
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>есть</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Способ нагрева
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>комбинированный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Конструкция
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>змеевик</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            Максимальная рабочая температура, °С
                                        </div>
                                    </td>
                                    <td>80</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <table class="table-mainParameters table-striped table-hover table-bordered" border="1">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="wrap-tip">
                                            Подводка
                                            <div class="tip">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                                <div class="tip-hov">
                                                    <div class="tip-tit">Подводка</div>
                                                    <div class="tip-text">Сторона прибора с которой производятся
                                                        подключение воды/ электричества/ газа
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>

                                    <td>боковая</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            Страна производитель
                                        </div>
                                    </td>

                                    <td>Польша</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            Тип водонагревателя
                                        </div>
                                    </td>

                                    <td>накопительный</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            Цвет
                                        </div>
                                    </td>

                                    <td>белый</td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            Гарантия, лет
                                        </div>
                                    </td>

                                    <td>5</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="wrap-modelRange">
                    <h3 class="bold">Модельный ряд Galmet TOWER</h3>
                    <div class="row modelRange">
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 100"> TOWER 100</a>
                            -
                            <span>27 300 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 1000"> TOWER 1000</a>
                            -
                            <span>161 500 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 140"> TOWER 140</a>
                            -
                            <span>31 000 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 1500"> TOWER 1500</a>
                            -
                            <span>218 400 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 200"> TOWER 200</a>
                            -
                            <span>38 000 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 300"> TOWER 300</a>
                            -
                            <span>45 800 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 400"> TOWER 400</a>
                            -
                            <span>61 000 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 500"> TOWER 500</a>
                            -
                            <span>68 600 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 720"> TOWER 720</a>
                            -
                            <span>122 800 р.</span><br>
                        </div>
                        <div class="col-sm-4 modelPrice">
                            <a href="#" class="grey" title="Бойлер косвенного нагрева  TOWER 120"> TOWER 120</a>
                            -
                            <span>28 900 р.</span>
                        </div>
                    </div>
                    <div class="modelRange-text">
                        <p>Интернет-магазин Энергомир предлагает широкий выбор горелочного оборудования, котлов и
                            агрегатов водяного и воздушного отопления, водонагревателей, насосов и другого
                            климатического оборудования. <a href="#">Выбрать электрокамины</a> по низким ценам.
                            Приобретенный товар доставляется по всей России.</p>
                        <p>Если бойлер косвенного нагрева Galmet TOWER есть в наличии, мы доставим его за 1 рабочий
                            день. Также вы можете заказать доставку на выбранную дату.</p>
                    </div>
                </div>
                <?php include '../../partials/earlier.php' ?>
            </div>
            <?php include '../../partials/goods.php' ?>
        </div>
    </div>

<?php include '../../partials/footer.php' ?>