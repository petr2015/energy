<?php include '../partials/header.php' ?>
    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a></li>
                <li class="breadCrumb-li">Связаться с нами</li>
            </ul>
            <div class="wrap-contact">
                <h3 class="bold">Связаться с нами</h3>
                <h2>Головной офис в Екатеринбурге</h2>
                <div class="contact-info">
                    <div class="contact-info-box">
                        <ul class="list-unstyled">
                            <li>
                                <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                <a class="dashed bold red" href="tel:+7 (343) 382-23-52">+7 (343) 382-23-52</a>
                                <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                            </li>
                            <li>
                                <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                <a class="dashed bold red" href="tel:+7 (343) 374-94-93">+7 (343) 374-94-93</a>
                                <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                            </li>
                            <li>
                                <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                ул. Студенческая, 51 - 427
                                <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                            </li>
                            <li>
                                <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                Пн - Пт 8:00 - 20:00
                            </li>
                            <li>
                                <i class="fa fa-envelope fa-fw" aria-hidden="true"></i>
                                <a class="dashed bold red" href="mailto:energomir@energomir.su">energomir@energomir.su</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <h2>Филиалы компании "Энергомир"</h2>
                <div class="contact-info">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="contact-info-box">
                                <span class="bold red">Челябинск</span><br>
                                <ul class="list-unstyled">
                                    <li>
                                        <i class="fa fa-phone fa-fw" aria-hidden="true"></i>
                                        +7 (351) 751-28-06
                                        <span class="grey fs12">(Пн - Пт 8:00 - 20:00)</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>
                                        ул. Хлебозаводская, дом 34
                                        <a href="#" class="map-link dashed fs12 grey scroll-link">На карте</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>
                                        Пн - Пт 9:00 - 19:00
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A1b656315ef08817c2de375452d1bda8cc1c7d5cf756b899446f69c14ad63c907&amp;width=1140&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
            <?php include '../partials/goods.php' ?>
        </div>
    </div>

<?php include '../partials/footer.php' ?>