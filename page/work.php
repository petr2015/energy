<?php include '../partials/header.php' ?>
    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a></li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span>Полезная информация по оборудованию интернет-магазина Энергомир</a></li>
                <li class="breadCrumb-li">Оплата и доставка</li>
            </ul>
            <div class="wrap-work">
                <h3 class="bold">Наши работы</h3>
                <div class="table-work">
                    <table align="left" border="0" cellpadding="0" cellspacing="0"
                           class="table table-bordered table-condensed table-striped" style="width:100%">
                        <tbody>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p style="text-align:center"><strong>НАИМЕНОВАНИЕ ОБЪЕКТА</strong></p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p style="text-align:center"><strong>ГОРОД</strong></p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p style="text-align:center"><strong>ОБОРУДОВАНИЕ</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px; width:689px">
                                <p style="text-align:center"><strong>2017</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px; width:689px">
                                <p style="text-align:center">Поставлено отопительное и теплогенерирующее оборудование
                                    общей мощностью более 112 МВт, в том числе:</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>Асфальто-бетонный завод</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>г. Курск</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p><a href="#">Горелка газовая Energy</a> мощностью 4,7 МВт для АБЗ</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:93px; width:162px">
                                <p>Трубный завод</p>
                            </td>
                            <td style="height:93px; width:158px">
                                <p>г. Полевской</p>
                            </td>
                            <td style="height:93px; width:369px">
                                <p><a href="#">Газовые инфракрасные обогреватели Carlieuklima</a> общей мощностью 260
                                    кВт для отопления гаража</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:70px; width:162px">
                                <p>Завод имени Серова</p>
                            </td>
                            <td style="height:70px; width:158px">
                                <p>г. Серов</p>
                            </td>
                            <td style="height:70px; width:369px">
                                <p><a href="#">Котлы Unical Modal</a> высокотемпературного исполнения с <a href="#">газовыми
                                        горелками FBR</a> - общая мощность котельной - 700 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>Комбинат «Электрохимприбор»</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>г. Лесной</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p><a href="#">Ультразвуковые увлажнители Туманит Сохра</a> – &nbsp;спец.исполнение</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Центр профессиональной патологии</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Ханты-Мансийск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Carlieuklima</a> - 7 шт. общей мощностью 284 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Машиностроительный завод</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Газовые горелки Cib UNIGAS - 2 шт. общей мощностью 5,4 МВт, <a href="#">газо-дизельная
                                        горелка Cib UNIGAS</a> 2,6 МВт с циркуляционными насосами Wilo</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Многоэтажный жилой дом</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Первоуральск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Радиаторы Purmo</a> - 144 шт.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:27px; width:162px">
                                <p>Центр МЧС</p>
                            </td>
                            <td style="height:27px; width:158px">
                                <p>г. Ханты-Мансийск</p>
                            </td>
                            <td style="height:27px; width:369px">
                                <p><a href="#">Газовые котлы Protherm Гризли</a> общей мощностью 400 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:82px; width:162px">
                                <p>Котельная</p>
                            </td>
                            <td style="height:82px; width:158px">
                                <p>г. Нефтеюганск</p>
                            </td>
                            <td style="height:82px; width:369px">
                                <p>Комплектация котельной предприятия на базе <a href="#">котлов Unical</a> общей
                                    мощностью 1,2 МВт с отечественными <a href="https://energomir.su/gorelki/mazutnie/">мазутными
                                        горелками</a> и <a
                                            href="https://energomir.su/gorelki/gazovie-2/gazovye-cib-unigas.html">газовыми
                                        горелками Cibital Unigas</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Экспериментальная установка переработки топлива</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Москва</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Мазутная горелка Baltur</a> &nbsp;мощностью 2 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>ПЫШМААВТОДОР</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p>Комплектация котельной <a href="#">котлами ICI Caldaie</a> с газовыми горелками
                                    Baltur общей мощностью 1 МВт, циркуляционными насосами Wilo и теплообменниками</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>Сельхозпредприятие</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>Свердловская обл.</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p><a href="#">Газовые горелки Baltur</a> мощностью 1,5 МВт для модернизации
                                    зерносушилки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>ЧАТО</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Котел водогрейный IVAR мощностью 1060 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>ЛУКОЙЛ-ПЕРМЬ</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>г. Пермь</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p><a href="#">Котлы ICI Caldaie</a> общей мощностью 0,4 МВт.</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px; width:689px">
                                <p style="text-align:center"><strong>2016</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:20px; width:689px">
                                <p style="text-align:center">Поставлено отопительное и теплогенерирующее оборудование
                                    общей мощностью более 70 МВт, в том числе:</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:27px; width:162px">
                                <p>Завод химического машиностроения</p>
                            </td>
                            <td style="height:27px; width:158px">
                                <p>Башкирия</p>
                            </td>
                            <td style="height:27px; width:369px">
                                <p><a href="#">Горелка газовая Cib Unigas</a> мощностью 1,6 МВт для печи,</p>

                                <p><a href="#">Горелка газовая Cib UNIGAS</a> мощностью 6,4 МВт для печи</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:54px; width:162px">
                                <p>Первая Оптовая Компания</p>
                            </td>
                            <td style="height:54px; width:158px">
                                <p>пос. Прохладный</p>
                            </td>
                            <td style="height:54px; width:369px">
                                <p><a href="#">Горелки на отработанном масле EnergyLogic</a> – 2 шт, общей мощностью 300
                                    кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Мясокомбинат</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Златоуст</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовые теплогенераторы Carlieuklima</a> подвесные в канальном исполнении
                                    - 7 шт, общей мощностью 180 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>Транспортное предприятие</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>г Сухой Лог</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p><a href="#">Котел на отработке Danvex</a> мощностью 150 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Бакар и К</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Муром</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Инфракрасные газовые обогреватели Carlieuklima</a>, общей мощностью 520
                                    кВт для отопления цеха</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Транспортная компания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Краснодарский край, п. Ильич</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котлы Lamborghini</a> &nbsp;мощностью 500 и 200 кВт, и <a href="#">горелка
                                        на отработке Ar-co</a> мощностью 460 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:20px; width:162px">
                                <p>Молочный завод</p>
                            </td>
                            <td style="height:20px; width:158px">
                                <p>Тюменская обл.</p>
                            </td>
                            <td style="height:20px; width:369px">
                                <p>Паровой котел &nbsp;ICI Caldaie с <a href="#">горелкой&nbsp;газовой&nbsp;Cib
                                        Unigas</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>ЗОЛОТО СЕВЕРНОГО УРАЛА</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Краснотурьинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Комбинированные горелки Lamborghini</a> газ-дизель - 2 шт. общей
                                    мощностью 1,2 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Оланк</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел Lamborghini MEGA PREX</a> мощностью 750 кВт с <a href="#">газовой
                                        горелкой Lamborghini</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Предприятие по утилизации отходов</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка на отработанном масле Nortec</a> мощностью 750 кВт для пиролизной
                                    установки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Мобильная нефтеподогревающая станция (МПС)</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Ямал</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Водогрейный котел Buderus</a> мощностью 1,8 МВт, <a href="#">водогрейные
                                        котлы ICI Caldaie</a> - 3 шт. общей мощностью 5,4 МВт&nbsp;&nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Офисное здание</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Электрические конвекторы Nobo</a> общей мощностью 37 кВт для отопления
                                    офиса</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Сельхозпредприятие</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>д. Прогресс, Тюменская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Baltur</a> мощностью 2,1 кВт для зерносушилки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производственный комплекс</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>пос. Михайловск, Свердловская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел на отработанном масле Unical с горелкой Euronord</a> мощностью 140
                                    кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Энергопромстрой</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка мазутная Cib UNIGAS</a> мощностью 1,3 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>КубаньДорСтрой</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Краснодар</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Energy</a> мощностью 8,2 МВт для сушильного барабана АБЗ
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Сельхозпредприятие</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>с. Лесниково, Курганская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Горелка газовая Cib UNIGAS мощностью 1,2 МВт для зерносушилки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:18px; width:162px">
                                <p>Частное лицо</p>
                            </td>
                            <td style="height:18px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:18px; width:369px">
                                <p><a href="#">Осушители воздуха Danvex</a> для бассейна</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:22px; width:689px">
                                <p style="text-align:center"><strong>2015</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Завод химического машиностроения</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Башкирия</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Cib Unigas</a> мощностью 1,6 МВт для печи – 3 шт.</p>

                                <p>&nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Теплоэнергокомпания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Чапаевск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Baltur</a> мощностью 16 МВт для районной котельной</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Энергопромстрой</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелки Alphatherm</a> для котлов – 2 шт. общей мощностью 2,3 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Диалог</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Миасс</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Carlieuklima</a> общей мощностью 250 кВт для отопления
                                    производственного помещения</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>ЗОЛОТО СЕВЕРНОГО УРАЛА</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Краснотурьинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котлы Lamborghini</a> – 2 шт. общей мощностью 1 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Кандинский Хаус</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел ICI Caldaie REX</a> &nbsp;мощностью 500 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Ямалкомплектсервис</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Лабытнанги</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Томир</a> общей мощностью 440 кВт с <a href="#">горелками
                                        на отработанном масле Danvex</a>. Для отопления и вентиляции цеха по
                                    производству пенопластовых изделий.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Тюменьдорстрой</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовые горелки Olympia</a>, общая мощность 470 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Инженерный Центр</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Baltur</a> мощностью 1200 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производство модульных зданий</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Инфракрасные обогреватели EnergoInfra</a> - 50 шт. общей мощностью 300
                                    кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Советский завод ЖБК</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Советский</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Инфракрасные газовые обогреватели Norgas</a>, 12 шт. общей мощностью 350
                                    кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Отопление теплиц</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Roberts Gordon</a> - 3 шт. общей мощностью 150 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Монтажная компания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка нефтяная IL-KA</a> мощностью 1,4 МВт, <a href="#">дизельная
                                        горелка Lamborghini</a> мощностью 924 кВт. <a
                                            href="https://energomir.su/gorelki/gorelki-baltur.html">Горелки Baltur</a>:
                                    газовая - мощностью 2,1 МВт, дизельная - 1,05 МВт для модернизации зерносушилок</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Нива</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>р.п. Павлоградка, Омская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Cib UNIGAS</a> мощностью 4,1 МВт для модернизации
                                    зерносушилки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Челябинский ЭлектроМеталлургический Комбинат</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Norgas</a> - 22 шт. для отопления</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Ишимагропродукт</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Ишимбай</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Baltur</a> мощностью 2,1 МВт для модернизации
                                    зерносушилки</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Котельная предприятия</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Канаш</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котлы Alphatherm</a> общей мощностью 450 кВт с <a href="#">газовыми
                                        горелками Alphatherm</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Котельная предприятия</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Лангепас</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Нефтяные горелки</a> - 2 шт., общей мощностью 460 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>МКАД</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Пятигорск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Energy</a> &nbsp;мощностью 15,7 МВт для модернизации АБЗ
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Клининговая компания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Промышленные осушители воздуха Danvex</a> - 4 шт.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Уралэнергостроймеханизация</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Котел на отработанном масле Danvex 250 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производственное предприятие</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Краснодар</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Дизельные горелки Lamborghini</a> для технологического оборудования
                                    обжига труб мощностью 4 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Молочный завод</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Тюменская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Cib UNIGAS</a> на 970 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:41px; width:689px">
                                <p style="text-align:center"><strong>2014</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Свердловский ЦППК</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Котел Alphatherm мощностью 240 кВт c <a href="#">горелкой на отработанном масле
                                        Euronord</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Трест Магнитострой</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>&nbsp;</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовые котлы Baxi Main Four</a> для систем поквартирного отопления
                                    жилого микрорайона, 360 штук</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Инмаркон</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Казахстан</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелки мазутные Олимпия</a> общей мощностью 3,2 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Косна</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Пермь</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Адсорбционые осушители воздуха Danvex</a> для ледовой арены</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Каменскволокно</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Г. Каменск-Шахтинский</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенератор Tecnoclima</a> мощностью 100 кВт с газовой горелкой
                                    Alphatherm</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>НТМК</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Нижний Тагил</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Тепловые пушки на магистральном газе KROLL</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Котельная министерства обороны</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Водогрейный котел Buderus</a> мощностью 1 МВт с газовой горелкой
                                    CibUnigas, 2 комплекта.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Ураласбест</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Асбест</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Горелка на отработанном масле Kroll на 900 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Модернизация сушильного барабана АБЗ</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Краснодарский край</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Горелка газовая Альфатерм мощностью 3,5 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Ника ПФ</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Челябинская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Ультразвуковые увлажнители Danvex</a> для мебельного производства</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Челябинский электрометаллургический комбинат</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Дизельные горелки Kiturami и ЗИП</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Трубная компания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Полевской</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Отопление производственного корпуса газовыми ИК-обогревателями Norgas</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Транспортная компания</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Теплогенератор и <a href="#">котел на отработанном масле</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Модернизация зерносушилок</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Тюменская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Газовые горелки ELCO 2 шт по 1,2 МВт каждая, дизельные горелки&nbsp;Lamborghini общей
                                    мощностью&nbsp; 2 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Частная пивоварня "ДУБИНИН"</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Ульяновская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котлы Олимпия</a> для производства</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:41px; width:689px">
                                <p style="text-align:center"><strong>2013</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Асфальтовый завод</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Краснодар</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовая горелка Alphatherm</a> мощностью 3,5 МВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Ростелеком</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Электроконвекторы Nobo</a> - 104 шт. для базовых станций Ростелеком</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Стройкомплекс</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Сатка</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка на отработанном масле Kroll</a> на 190 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Строительная компания, комплектация объекта</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Бойлер Austria Email</a> объемом 500 литров - 3 шт.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Челябинский электрометаллургический комбинат</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовый воздухонагреватель Norgas</a> - 8 шт. общей мощностью 120 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Сушка семян подсолнечника</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Орск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенератор Kroll</a> – на 92 кВт в специальном исполнении</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Завод ЖБИ</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Оренбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Газовые воздухонагреватели Norgas общей мощностью 270 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Котельная министерства обороны</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Водогрейный котел Buderus</a> мощностью 1 МВт с газовой горелкой
                                    CibUnigas, 2 комплекта.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>УралГранитСтройГрупп</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Тепловые завесы Тепломаш</a> - 4 шт. общей мощностью 190 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производственное здание (производство трансформаторов)</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Асбест</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Котел Alphatherm на 100 кВт с <a href="#">горелкой Euronord EcoLogic</a> на
                                    отработке.</p>

                                <p>Котельная на базе <a href="#">котла Danvex на отработанном масле</a> с горелкой
                                    мощностью 30 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Объекты компании «Стройкомплекс»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Сатка, Челябинской обл</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел ICI Caldaie REX</a> с горелкой Kroll на отработанном масле - 2
                                    комплекта, мощностью 200 кВт каждый</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Центр Физических Исследований и Экспериментальных Технологий</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Ecoflam</a> мощностью 700 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Объекты компании РеалСтройГрупп</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Челябинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Твердотопливный котел «Универсал» мощностью 160 кВт, котлы Alphatherm с <a href="#">горелками
                                        на отработанном масле Euronord EcoLogic</a> общей мощностью 240 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Модернизация зерносушилок</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Тюмень</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелки газовые ELCO</a> - 3 шт. по 850 кВт каждая, 1 шт. мощностью 2
                                    МВт.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>ВВК Январь</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Верхнеуральск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Горелка газовая Alphatherm</a> на 750 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производственные площади</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Абакан</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Euronord</a> и котел Alphatherm с горелками Kroll для
                                    работы на отработанном масле и <a href="#">тепловентиляторами Тепломаш</a></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Объекты ОАО «Газпром» (Энергомашкомплект)</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>ХМАО, ЯНАО</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Мобильные <a href="#">теплогенераторы Kroll</a> 4 шт. общей мощностью 270 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>НПО Сад и огород</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Челябинская обл</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Теплогенераторы Euronord</a> с дизельными горелками Riello Gulliver общей
                                    мощностью 480 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Нагайбакский птицеводческий комплекс</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Челябинская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел ICI REX</a> &nbsp;мощностью 300 кВт с газовой горелкой Alphatherm
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Производственное помещение</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Ставрополь</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Теплогенератор Tecnoclima с <a href="#">газовой горелкой Lamborghini</a> &nbsp;мощностью
                                    250 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height:41px; width:689px">
                                <p style="text-align:center"><strong>2012</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Компания «Стройкомплект»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Котел Unical Modal</a> &nbsp;мощностью 180 кВт с горелкой на отработанном
                                    масле Kroll – 2 комплекта</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Рыбный цех компании «Фишка»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>с. Верхнее Дуброво Свердловской области</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Котельная для работы на отработанном масле на базе <a href="#">котла Lamborghini</a>
                                    на 100 кВт с универсальной &nbsp;горелкой Kroll</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>«МЕГА»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Электроконвекторы Dimplex</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>«Когалымнефтегаз»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Когалым</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Электроконвекторы NOBO</a> для отопления буровых установок (37 шт.)</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Спортивные комплексы</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Красноярский край</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Теплогенераторы Tecnoclima с горелками на отработанном масле&nbsp; Euronord Ecologic
                                    общей мощностью 330 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px;width:162px;">
                                <p>«РОНА»</p>
                            </td>
                            <td style=" height:41px; width:158px;">
                                <p>г. Тольятти</p>
                            </td>
                            <td style="height:41px; width:369px;">
                                <p><a href="#">Горелка универсальная Kroll</a> для работы на отработанном масле и любом
                                    жидком топливе на 900 кВт</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Склад компании СладКо</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Екатеринбург</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Инфракрасные обогреватели Energotech</a> - 23 шт., общей мощностью 138
                                    кВт с автоматикой и тепловые завесы Aerotek (6 шт)</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Буровая платформа «Сахалинтефтегазсервис»</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>г. Южно-Сахалинск</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p>Теплогенераторы Euronord SKE 60 с дизельными горелками Alphatherm</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:41px; width:162px">
                                <p>Кафе Хуторок</p>
                            </td>
                            <td style="height:41px; width:158px">
                                <p>Тюменская обл.</p>
                            </td>
                            <td style="height:41px; width:369px">
                                <p><a href="#">Газовые инфракрасные обогреватели Neoclima</a> - 15 шт, общей мощностью
                                    180 кВт</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h3 class="bold">Задайте вопрос нашим специалистам и получите ответ прямо здесь</h3>
                <div class="pd-mb bold">Чтобы оставлять комментарии необходимо войти</div>
                <div class="work-seti">
                    <div class="text">Войти с помощью:</div>
                    <div id="uLogin" data-ulogin="display=small;theme=classic;fields=first_name,last_name;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2F;mobilebuttons=0;"></div>
                </div>
            </div>
            <?php include '../partials/goods.php' ?>
        </div>
    </div>

<?php include '../partials/footer.php' ?>