<?php include '../partials/header.php' ?>

    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li">Корзина покупок</li>
            </ul>
            <div class="wrap-maxCard ">
                <h3 class="bold">Оформление заказа</h3>
            </div>
            <div class="wrap-basket1">
                <div class="row">
                    <div class="col-sm-12 col-lg-8">
                        <div class="basket-lef-bord">
                            <h3 class="blue"><span class="fs13">1 шаг:</span> Проверьте товары в заказе</h3>
                            <div class="basket-divTable">
                                <div class="divTable-title">
                                    <div class="divTable-box bold">Фото</div>
                                    <div class="divTable-box bold">Название</div>
                                    <div class="divTable-box bold">Количество</div>
                                    <div class="divTable-box bold">Стоимость</div>
                                    <div class="divTable-box bold">Итого</div>
                                    <div class="divTable-box"></div>
                                </div>
                                <div class="divTable-card">
                                    <div class="divTable-box">
                                        <div class="divTable-img">
                                            <img src="/assets/img/1.png" title="" alt="">
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-name">
                                            <a href="#">Водонагреватель электрический Эван В1 9</a>
                                            <span class="cod"><span class="bold xlNone">id</span><span class="mobNone">Код товара</span>: 18134</span>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="numberInp">
                                            <button class="btn white btnLef"><i class="fa fa-minus-circle"></i></button>
                                            <input type="text" placeholder="1">
                                            <button class="btn white btnRig"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Цена:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Сумма:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box"><i class="fa fa-times fa-times-click"
                                                                 aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="divTable-card">
                                    <div class="divTable-box">
                                        <div class="divTable-img">
                                            <img src="/assets/img/1.png" title="" alt="">
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-name">
                                            <a href="#">Водонагреватель электрический Эван В1 9</a>
                                            <span class="cod"><span class="bold xlNone">id</span><span class="mobNone">Код товара</span>: 18134</span>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="numberInp">
                                            <button class="btn white btnLef"><i class="fa fa-minus-circle"></i></button>
                                            <input type="text" placeholder="1">
                                            <button class="btn white btnRig"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Цена:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Сумма:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box"><i class="fa fa-times fa-times-click"
                                                                 aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="divTable-card">
                                    <div class="divTable-box">
                                        <div class="divTable-img">
                                            <img src="/assets/img/1.png" title="" alt="">
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-name">
                                            <a href="#">Водонагреватель электрический Эван В1 9</a>
                                            <span class="cod"><span class="bold xlNone">id</span><span class="mobNone">Код товара</span>: 18134</span>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="numberInp">
                                            <button class="btn white btnLef"><i class="fa fa-minus-circle"></i></button>
                                            <input type="text" placeholder="1">
                                            <button class="btn white btnRig"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Цена:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box">
                                        <div class="divTable-price"><span class="xlNone">Сумма:</span>117 015 р.</div>
                                    </div>
                                    <div class="divTable-box"><i class="fa fa-times fa-times-click"
                                                                 aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="basket-rig-prise xlNone">
                                    <div class="dib">Сумма:</div>
                                    <div class="bold dib">1231230 р.</div>
                                </div>
                                <div class="basket-rig-prise xlNone">
                                    <div class="dib">Комиссия за способ оплаты:</div>
                                    <div class="bold dib">13123123 р.</div>
                                </div>
                                <div class="basket-rig-prise xlNone">
                                    <div class="dib">Доставка:</div>
                                    <div class="bold dib">1230 р.</div>
                                </div>
                                <div class="basket-rig-prise xlNone">
                                    <div class="dib">Итого:</div>
                                    <div class="bold dib">123150 р.</div>
                                </div>
                            </div>
                        </div>
                        <!-- basket-lef-bord появляется в какихто моментах-->
                        <div class="basket-lef-bord">
                            <a href="#" class="dashed-link dib">Скрыть дополнительные товары</a>
                            <div class="owl-carousel owl-basket">
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Стабилизатор напряжения</div>
                                        <div class="card-link">Бастион Teplocom ST 555-И</div>
                                        <div class="card-price bold">+ 4 300 р.</div>
                                        <button class="btn white">Добавить к заказу</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Стабилизатор напряжения</div>
                                        <div class="card-link">Бастион Teplocom ST 555-И</div>
                                        <div class="card-price bold">+ 4 300 р.</div>
                                        <button class="btn white">Добавить к заказу</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Стабилизатор напряжения</div>
                                        <div class="card-link">Бастион Teplocom ST 555-И</div>
                                        <div class="card-price bold">+ 4 300 р.</div>
                                        <button class="btn white">Добавить к заказу</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Стабилизатор напряжения</div>
                                        <div class="card-link">Бастион Teplocom ST 555-И</div>
                                        <div class="card-price bold">+ 4 300 р.</div>
                                        <button class="btn white">Добавить к заказу</button>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="card">
                                        <a href="#" class="absLink"></a>
                                        <div class="card-img">
                                            <img src="/assets/img/img2.png" title="" alt="">
                                        </div>
                                        <div class="special-text">Стабилизатор напряжения</div>
                                        <div class="card-link">Бастион Teplocom ST 555-И</div>
                                        <div class="card-price bold">+ 4 300 р.</div>
                                        <button class="btn white">Добавить к заказу</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="basket-lef-bord">
                            <h3 class="blue"><span class="fs13">2 шаг:</span>Укажите контактные данные</h3>
                            <p>Заполняя формы, вы соглашаетесь с <a href="#">политикой конфиденциальности</a></p>
                            <div class="form-basket">
                                <form action="#">
                                    <div class="form-basket-line row">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                        <span class="basket-line-name">
                                            Имя
                                            <span class="font-en-red">*</span>
                                        </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <input type="text" placeholder="Контактное лицо">
                                                <p class="inp-help">Пример: Иванов Сергей</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-basket-line row">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                            <div class="basket-line-name">e-mail</div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <input type="email" placeholder="Почтовый ящик">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-basket-line row">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                            <div class="basket-line-name">
                                                Телефон
                                                <span class="font-en-red">*</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <input type="text" class="phone" placeholder="+7(912)34-56-789">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-basket-line row">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold"></div>
                                        <div class="col-sm-7">
                                            <label for="client" class="checkboxLabel">
                                                <input type="checkbox" id="client">
                                                Покупаю для организации
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-basket-line row hidden">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                            <div class="basket-line-name">
                                                Название
                                                <span class="red">*</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <input type="text" placeholder="Название организации">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-basket-line row hidden">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                            <div class="basket-line-name">
                                                ИНН
                                                <span class="font-en-red">*</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <input type="text" placeholder="ИНН">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-basket-line row">
                                        <div class="col-sm-12 col-md-3 col-xl-2 bold">
                                            <div class="basket-line-name">Комментарий</div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="inp">
                                                <textarea name="" placeholder=""></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="basket-lef-bord">
                            <h3 class="blue"><span class="fs13">3 шаг:</span>Выберите способ доставки</h3>
                            <div class="form-basket delivery">
                                <div class="delivery-box">
                                    <span>Доставка в регион:</span>
                                    <div class="inp">
                                        <input type="text" placeholder="Екатеринбург">
                                    </div>
                                </div>
                                <div class="delivery-box">
                                    <div class="bold mb1">Самовывоз</div>
                                    <label class="radio-inline" for="shipping_method1">
                                        <input type="radio" id="shipping_method1" name="shipping_method">
                                        <span class="fs12">Самовывоз c главного склада (Екатеринбург)</span>
                                        <span class="fon">Бесплатно</span>
                                    </label>
                                </div>
                                <div class="delivery-box">
                                    <div class="bold mb1">Доставка</div>
                                    <label class="radio-inline" for="shipping_method">
                                        <input type="radio" id="shipping_method" name="shipping_method">
                                        <span class="fs12">Доставка по Екатеринбургу</span>
                                        <span class="fon">Бесплатно</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="basket-lef-bord">
                            <h3 class="blue"><span class="fs13">4 шаг:</span>Выберите способ оплаты</h3>
                            <div class="form-basket ">
                                <p class="fs12">Любой заказ должен быть подтвержден менеджером согласно условиям работы
                                    магазина. Если вы выбираете оплату с помощью карты или электронных платежных систем,
                                    то
                                    после подтверждения заказа с нашей стороны, вы получите ссылку на страницу с формой
                                    оплаты. Мы обеспечиваем полную безопасность ваших платежных данных, не сохраняя их
                                    на
                                    своих серверах. Оплата картой производится на платёжных шлюзах ВТБ24 или Сбербанка,
                                    электронными деньгами - на платежном шлюзе Яндекс.Касса.</p>
                                <!-- посмотреть как работает на сайте-->
                                <div class="delivery-box">
                                    <label class="radio-inline" for="payment_method2">
                                        <input type="radio" id="payment_method2" name="payment_method">
                                        <span class="fs12">При получении</span>
                                        <span class="fs12 db grey">Без комиссий. Доступно не для всех видов товаров.
                                        Возможность оплаты при получении подтверждается менеджером на основании Вашей заявки.</span>
                                    </label>
                                </div>
                                <div class="delivery-box">
                                    <label class="radio-inline" for="payment_method1">
                                        <input type="radio" id="payment_method1" name="payment_method">
                                        <span class="fs12">По квитанции (счету) в любом банке</span>
                                        <span class="fs12 db grey">Наличие и размер комиссии уточняйте в банке</span>
                                    </label>
                                </div>
                                <div class="delivery-box">
                                    <label class="radio-inline" for="payment_method">
                                        <input type="radio" id="payment_method" name="payment_method">
                                        <span class="fs12">Пластиковая карта Visa / MasterCard</span>
                                        <span class="fs12 db grey">Комиссия 1.2%</span>
                                    </label>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-4">
                        <div class="basket-rig">
                            <div class="basket-rig-title">Ваш заказ</div>
                            <div class="basket-rig-box">
                                <div class="basketCard">
                                    <div class="basketCard-img">
                                        <img src="/assets/img/1.png" title="" alt="">
                                    </div>
                                    <div class="basketCard-text">
                                        <div class="fs13">Водонагреватель электрический Galmet VULCAN 60</div>
                                        <div class="grey fs13">х1</div>
                                    </div>
                                    <div class="basketCard-price bold fs13">13 800 р.</div>
                                </div>
                                <div class="basket-rig-prise mt1">
                                    <div class="fs13 dib">Сумма:</div>
                                    <div class="bold dib">0 р.</div>
                                </div>
                                <div class="basket-rig-prise mb1">
                                    <div class="fs13 dib">Итого:</div>
                                    <div class="bold dib">0 р.</div>
                                </div>
                                <!--  появляется в случае невведенного поля ввода +  -->
                                <ol class="form-none">
                                    <li>Укажите контактное лицо</li>
                                    <li>Укажите номер телефона</li>
                                    <li>Укажите способ доставки</li>
                                    <li>Укажите способ оплаты</li>
                                </ol>
                                <!--  появляется в случае невведенного поля ввода +  -->
                                <button class="btn green width bold">Оформить заказ <i class="fa fa-shopping-cart"
                                                                                       aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="privacy">
                <h3 class="blue">Условия соглашения и политика конфиденциальности</h3>
                <div class="privacy-scrollable pre-scrollable">
                    <h2>Условия соглашения</h2>
                    <h2>Сфера ответственности</h2>
                    <p><strong>Компания Энергомир не несет ответственности за:</strong></p>
                    <ul>
                        <li>абсолютную точность информации, касающуюся товаров или услуг, которую предоставляют ей
                            другие компании;
                        </li>
                        <li>проблемы, которые могут возникнуть вследствие неверного использования информации,
                            находящейся на сайте energomir.su;
                        </li>
                        <li>последствия, возникшие в результате невозможности применения информации, находящейся на
                            сайте energomir.su.
                        </li>
                    </ul>
                    <p>Характеристики инструментов, которые представлены в нашем магазине, подвергаются постоянным
                        изменениям, поэтому описание конкретного товара на сайте может не совпадать с реальными
                        параметрами изделия, находящегося в продаже.<br>
                        Во избежание спорных ситуаций, требуйте дополнительной информации во время размещения заказа.
                    </p>
                    <p>Сайт <a href="/">energomir.su</a> допускает наличие ошибочной либо неточной
                        информации, находящейся на сайте. Чтобы избежать недоразумений, получайте информацию о
                        интересующем вас товаре на сайте производителя.</p>
                    <p><a href="/">energomir.su</a> вправе корректировать, удалять, изменять и
                        добавлять информацию, опираясь на заинтересованность партнеров и корпоративные интересы.</p>
                    <h2>Юридические основания</h2>
                    <p>Информация, указанная на сайте, не является публичной офертой. Информация о товарах, их
                        технических свойствах и характеристиках, ценах является предложением сайта Энергомир от лица
                        организаций-партнеров делать оферты. Акцептом сайта Энергомир полученной от пользователя оферты
                        является подтверждение заказа в виде ссылки на форму оплаты, счета на оплату или подтверждения в
                        любой письменной форме с указанием товара и его цены. Сообщение Энергомир о цене заказанного
                        товара, отличающейся от указанной в оферте, является отказом Энергомир от акцепта и одновременно
                        офертой со стороны Энергомир. Информация о технических характеристиках товаров, указанная на
                        сайте, приводится для ознакомления и может быть изменена производителем в одностороннем порядке.
                        Для уточнения характеристик просим запрашивать актуальный паспорт оборудования. Изображения
                        товаров на фотографиях, представленных в каталоге на сайте, могут отличаться от оригиналов.
                        Информация о цене товара, указанная в каталоге на сайте, может отличаться от фактической к
                        моменту оформления заказа на соответствующий товар. Подтверждением цены заказанного товара
                        является сообщение Энергомир о цене такого товара.</p>
                    <p>Оплата доставки производится клиентом непосредственно транспортной компании. Указанная стоимость
                        доставки рассчитывается автоматически на основании габаритов и веса Товара, является
                        ориентировочной и зависит от фактических транспортных габаритов изделия и пожеланий клиента о
                        способах доставки товара и может быть как больше, так и меньше указанной.</p>
                    <p>Продавец в одностороннем порядке принимает и изменяет условия Соглашения. В отношениях между
                        Продавцом и Покупателем применяются положения Соглашения, действующие с того момента, как
                        Покупатель зарегистрировался или начал использование Сайта для совершения данного заказа.
                        Продавец оставляет за собой полное и безоговорочное право любым образом в одностороннем порядке
                        модифицировать, то есть изменять, дополнять, удалять и другим образом корректировать любые
                        пункты и части пунктов Соглашения без предварительного оповещения Покупателя. Но это не является
                        основанием для отказа Продавца от обязательств по уже сделанным Покупателем Заказам.</p>
                    <p>Настоящее Соглашение вступает в силу с момента регистрации Покупателя на Сайте или обращения
                        к Продавцу для приобретения Товара.</p>
                    <p>Моментом обращения считается момент, когда Покупатель фактически начал осуществлять действия,
                        направленные на приобретение Товара у Продавца.</p>
                    <h2>Информация о Товаре</h2>
                    <p>Фотообразцы имитируют представленный на Сайте Товар. Реальный вид Товара может не
                        совпадать с изображением, представленным на Сайте. Каждый фотообразец сопровождается
                        текстовой информацией: артикулом, ценой и описанием Товара.</p>
                    <p>Компания оставляет за собой право (однако не обязана) осуществлять предварительную
                        проверку, просматривать, помечать, выбирать, изменять или убирать любое содержание
                        Сайта.</p>
                    <p>При приобретении Покупателем технически сложных товаров, которые требуют
                        специализированной установки, Продавец не несет ответственности за правильность их
                        подключения и использования, за исключением случаев, в которых Покупатель пользуется
                        услугами Интернет-магазина.</p>
                    <h3>Возврат товара</h3>
                    <p>Процедура возврата товара регламентируется статьей 26.1 федерального закона «О защите прав
                        потребителей».</p>
                    <ul>
                        <li>Потребитель вправе отказаться от товара в любое время до его передачи, а после передачи
                            товара - в течение семи дней;
                        </li>
                        <li>Возврат / обмен товара с выявленным в процессе эксплуатации скрытым производственными
                            дефектом производится на основаниях и в сроки, установленные Законом «О защите прав
                            потребителей»
                        </li>
                        <li>Возврат товара надлежащего качества возможен в случае, если сохранены его товарный вид,
                            потребительские свойства, а также документ, подтверждающий факт и условия покупки указанного
                            товара;
                        </li>
                        <li>Потребитель не вправе отказаться от товара надлежащего качества, имеющего
                            индивидуально-определенные свойства, если указанный товар может быть использован
                            исключительно приобретающим его человеком;
                        </li>
                        <li>При отказе потребителя от товара продавец должен возвратить ему денежную сумму, уплаченную
                            потребителем по договору, за исключением расходов продавца на доставку от потребителя
                            возвращенного товара, не позднее чем через десять дней со дня предъявления потребителем
                            соответствующего требования;
                        </li>
                    </ul>
                    <p>ВНИМАНИЕ! Покупателям газового оборудования:<br>
                        Законодательством РФ предусмотрены следующие случаи и основания возврата газового оборудования:
                    </p>
                    <p>• Отказ от заказа, если доставка еще не осуществлена: Покупатель вправе отказаться от товара в
                        любое время до его передачи (пункт 21 Правил продажи товаров дистанционным способом,
                        утвержденных Постановлением Правительства РФ от 27.09.2007 № 612).</p>
                    <p>• Возврат газового оборудования надлежащего качества, если доставка уже осуществлена, не
                        производится, поскольку бытовое газовое оборудование включено в Перечень непродовольственных
                        товаров надлежащего качества, не подлежащих возврату и обмену на аналогичный товар других
                        размеров, формы, габаритов, расцветки или комплектации, утвержденный Постановлением
                        Правительства РФ от 19.01.1998 № 55.</p>
                    <p>• Возврат газового оборудования ненадлежащего качества:</p>
                    <p>Если покупателю продан товар ненадлежащего качества, то он вправе требовать:</p>
                    <p>- безвозмездного устранения недостатков товара или возмещения расходов на их исправление
                        покупателем или третьим лицом;</p>
                    <p>- соразмерного уменьшения покупной цены;</p>
                    <p>- замены на товар аналогичной марки (модели, артикула) или на такой же товар другой марки
                        (модели, артикула) с соответствующим перерасчетом покупной цены;</p>
                    <p>- отказаться от исполнения договора и потребовать возврата уплаченной за товар суммы (пункты
                        28-29 Правил продажи товаров дистанционным способом, утвержденных Постановлением Правительства
                        РФ от 27.09.2007 № 612).</p>

                    <p>&nbsp;</p>

                    <p>Правилами продажи товаров дистанционным способом (пункты 21, 34 – 36) предусмотрен следующий
                        порядок возврата уплаченных денежных средств:</p>

                    <p>При отказе покупателя от товара продавец должен возвратить ему сумму, уплаченную покупателем в
                        соответствии с договором, за исключением расходов продавца на доставку от покупателя
                        возвращенного товара, не позднее чем через 10 дней с даты предъявления покупателем
                        соответствующего требования.</p>

                    <p>В случае если возврат суммы, уплаченной покупателем в соответствии с договором, осуществляется
                        неодновременно с возвратом товара покупателем, возврат указанной суммы осуществляется продавцом
                        с согласия покупателя одним из следующих способов:</p>

                    <p>а) наличными денежными средствами по месту нахождения продавца;</p>

                    <p>б) почтовым переводом;</p>

                    <p>в) путем перечисления соответствующей суммы на банковский или иной счет покупателя, указанный
                        покупателем.</p>

                    <br>

                    <p>Расходы на осуществление возврата суммы, уплаченной покупателем в соответствии с договором, несет
                        продавец. Оплата товара покупателем путем перевода средств на счет третьего лица, указанного
                        продавцом, не освобождает продавца от обязанности осуществить возврат уплаченной покупателем
                        суммы при возврате покупателем товара как надлежащего, так и ненадлежащего качества.</p>

                    <p>Возврат денежных средств Покупателю производится в соответствии с действующим законодательством
                        РФ, но в любом случае не позднее 30 календарных дней со дня получения соответствующего заявления
                        Покупателя в письменном виде или по электронной почте.</p>

                    <p>Покупатель несет ответственность за достоверность указанных им в заявлении реквизитов для
                        возврата денежных средств.</p>

                    <h3>Оплата электронными деньгами</h3>

                    <p>Предоставляемая вами персональная информация (имя, адрес, телефон, e-mail, номер кредитной карты)
                        является конфиденциальной и не подлежит разглашению. Данные вашего заказа передаются только в
                        зашифрованном виде и не сохраняются на нашем Web-сервере.</p>

                    <p>Безопасность обработки Интернет-платежей с электронными деньгами гарантирует Яндекс.Касса.</p>

                    <p>В случае возникновения вопросов по поводу конфиденциальности операций с платёжными картами и
                        предоставляемой вами информации вы можете связаться со службой технической поддержки
                        Яндекс.Касса.</p>

                    <p>Для оплаты вы будете перенаправлены на страницу Яндекс.Касса.</p>

                    <p>Транзакция может занять около 40 секунд. Дождитесь завершения операции. Не нажимайте повторно
                        кнопку «Заплатить».</p>

                    <p>Платеж происходит в режиме реального времени и зачисляется в течение 15 минут.</p>

                    <h3>Оплата пластиковыми картами</h3>

                    <p>Предоставляемая вами персональная информация (имя, адрес, телефон, e-mail, номер кредитной карты)
                        является конфиденциальной и не подлежит разглашению. Данные вашей кредитной карты передаются
                        только в зашифрованном виде и не сохраняются на нашем Web-сервере.</p>

                    <p>Безопасность обработки Интернет-платежей по пластиковым картам гарантирует банк-эквайер. Все
                        операции с картами происходят в соответствии с требованиями VISA International, MasterCard и
                        других платежных систем. При передаче информации используются специальные технологии
                        безопасности карточных онлайн-платежей, обработка данных ведется на безопасном
                        высокотехнологичном процессинговом сервере.</p>

                    <p>В случае возникновения вопросов по поводу конфиденциальности операций с платёжными картами и
                        предоставляемой вами информации вы можете связаться со службой технической поддержки банка.</p>

                    <p>На странице авторизации потребуется ввести номер карты, имя владельца карты, срок действия карты,
                        верификационный номер карты (CVV2 для VISA или CVC2 для MasterCard). Все необходимые данные
                        пропечатаны на самой карте. Верификационный номер карты — это три цифры, находящиеся на обратной
                        стороне карты.</p>

                    <p>Для оплаты вы будете перенаправлены на страницу банка.</p>

                    <p>Произвести оплату необходимо в течение 15 минут после перехода на страницу авторизации
                        карточки.</p>

                    <p>Транзакция может занять около 40 секунд. Дождитесь завершения операции. Не нажимайте повторно
                        кнопку «Заплатить».</p>

                    <p>Платеж происходит в режиме реального времени и зачисляется в течение 15 минут.</p>

                    <br>

                    <h2>Политика конфиденциальности</h2>

                    <p>Политика конфиденциальности</p>

                    <ol>
                        <li>Общие положения</li>
                    </ol>

                    <p>Настоящие Положение о политике конфиденциальности (далее — Положение) является официальным
                        документом ООО «Мир горелок», расположенного по адресу: 620078 г. Екатеринбург, ул. Студенческая
                        51-427 (далее — «Компания»/ «Оператор»), и определяет порядок обработки и защиты информации о
                        физических лицах (далее — Пользователи), пользующихся сервисами, информацией, услугами,
                        программами (в т.ч. программами лояльности) и продуктами сайта, расположенного по адресу
                        energomir.su (далее — Сайт). Соблюдение конфиденциальности важно для Компании, ведь целью данной
                        Политики конфиденциальности является обеспечение защиты прав и свобод человека и гражданина при
                        обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни,
                        личную и семейную тайну, от несанкционированного доступа и разглашения. Политика
                        Конфиденциальности описывает, как осуществляется обработка персональных данных — любые действия
                        (операции) или совокупность действий (операций), совершаемых с использованием средств
                        автоматизации или без использования таких средств с персональными данными, включая сбор, запись,
                        систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение,
                        использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование,
                        удаление, уничтожение персональных данных. Отношения, связанные с обработкой персональных данных
                        и информации о пользователях Сайта, регулируются настоящим Положением, иными официальными
                        документами Оператора и действующим законодательством РФ. Обработка персональных данных
                        осуществляется на законной и справедливой основе, разумно и добросовестно на основе
                        принципов:</p>

                    <ul>
                        <li>законности целей и способов обработки персональных данных;</li>
                        <li>добросовестности;</li>
                        <li>соответствия целей обработки персональных данных целям, заранее определенным и заявленным
                            при сборе персональных данных, а также полномочиям Компании;
                        </li>
                        <li>соответствия объема и характера обрабатываемых персональных данных, способов обработки
                            персональных данных целям обработки персональных данных.
                        </li>
                    </ul>

                    <p>Настоящая Политика Конфиденциальности регулирует любой вид обработки персональных данных и
                        информации личного характера (любой информации, позволяющей установить личность, и любой иной
                        информации, связанной с этим) о физических лицах, которые являются потребителями продукции или
                        услуг Компании. Настоящая Политика распространяется на обработку личных, персональных данных,
                        собранных любыми средствами, как активными, так и пассивными, как через Интернет, так и без его
                        использования, от лиц, находящихся в любой точке мира.</p>

                    <ol start="2">
                        <li>Сбор персональных данных</li>
                    </ol>
                    <p>Целью обработки персональных данных является выполнения обязательств Оператора перед
                        Пользователями в отношении использования Сайта и его сервисов. Обработка персональных данных
                        пользователей осуществляется с согласия субъекта персональных данных на обработку его
                        персональных данных. Под персональными данными понимается любая информация, относящаяся к прямо
                        или косвенно определенному или определяемому физическому лицу (субъекту персональных данных) и
                        которая может быть использована для идентификации определенного лица либо связи с ним. Запрос
                        персональных данных Пользователя может осуществляться в любой момент, в том числе, когда
                        Пользователь связывается с Компанией. Компания может использовать такие данные в соответствии с
                        настоящей Политикой Конфиденциальности. Она также может совмещать такую информацию с иной
                        информацией для целей предоставления и улучшения своих продуктов, услуг, информационного
                        наполнения (контента) и коммуникаций. Ниже приведены некоторые примеры типов персональных
                        данных, которые Компания может собирать, и как может использовать такую информацию.</p>
                    <p>Сбор персональных данных</p>
                    <p>Собираются различные данные/информация, включая:</p>
                    <ul>
                        <li>Имя, фамилия, отчество</li>
                        <li>почтовый адрес</li>
                        <li>номер телефона</li>
                        <li>адрес электронной почты;</li>
                    </ul>
                    <p>Персональные данные могут также включать в себя дополнительно предоставляемые Пользователями по
                        запросу Оператора в целях исполнения Оператором обязательств перед Пользователями, вытекающих из
                        договора на поставку товаров либо оказание услуг. Оператор вправе, в частности, запросить у
                        Пользователя данные документа, удостоверяющего личность, для оформления такого договора. При
                        обработке персональных данных Компанией обеспечивается точность персональных данных, их
                        достаточность, а в необходимых случаях и актуальность по отношению к целям обработки
                        персональных данных.</p>
                    <ol start="3">
                        <li>Хранение и использование персональных данных</li>
                    </ol>
                    <p>Персональные данные Пользователей хранятся на электронных и/или бумажных носителях и
                        обрабатываются с использованием автоматизированных и неавтоматизированных систем. Использование
                        персональной информации<br>
                        Собираемые персональные предназначаются для идентификации Пользователей при переговорах и
                        передаче товаров, а также для выполнения обязательств по доставке товаров. Такие персональные
                        данные, как имя, фамилия, отчество и адрес могут быть переданы транспортным компаниям при
                        доставке товаров для идентификации Пользователей. Они также помогают Компании улучшать свои
                        услуги, контент и коммуникации<strong>. Компания не использует данные Пользователей для
                            осуществления рекламных рассылок</strong>. Время от времени Компания может использовать
                        персональные данные Пользователя для отправки важных уведомлений, содержащих информацию об
                        изменениях положений, условий и политик, а также подтверждающих размещенные заказы/ совершенные
                        покупки/заказанные услуги. Поскольку такая информация важна для взаимоотношений с Компанией,
                        Пользователь не может отказаться от получения таких сообщений. Компания также может использовать
                        персональную информацию для внутренних целей, таких как: проведение аудита, анализ данных и
                        различных исследований в целях улучшения продуктов и услуг Компании, а также взаимодействие с
                        потребителями. Если Пользователь принимает участие в розыгрыше призов, конкурсе или похожем
                        стимулирующем мероприятии, Компания сохраняет за собой право использовать предоставляемые
                        персональные данные для управления такими программами.</p>
                    <p>Сбор и использование информации, не являющейся персональной</p>
                    <p>Также собираются данные, не являющиеся персональными − данные, не позволяющие прямо ассоциировать
                        их с каким-либо определённым лицом. Компания может собирать, использовать, передавать и
                        раскрывать информацию, не являющуюся персональной, для любых целей. Ниже приведены примеры
                        информации, не являющейся персональной, которую собирает Кампания, и как она может её
                        использовать: Компания можем собирать персональные данные, такие как: сведения о роде занятий,
                        языке, почтовом индексе, уникальном идентификаторе устройства, местоположении и временной зоне,
                        в которой используется тот или иной продукт, для того чтобы лучше понимать поведение
                        потребителей и улучшать свои продукты, услуги и коммуникации. Компания может собирать
                        персональные данные/информацию о том, чем интересуется пользователь на веб-сайте Компании при
                        использовании других продуктов и сервисов. Такие данные/информация собирается и используется для
                        того, чтобы помочь Компании предоставлять более полезную информацию своим клиентам и для
                        понимания того, какие элементы сайта, продуктов и услуг наиболее интересны. Для целей настоящей
                        Политики Конфиденциальности совокупные данные рассматриваются как данные/информация, не
                        являющиеся персональными. Если совмещается информация, не являющуюся персональной, с
                        персональной информацией, такая совокупная информация будет рассматриваться как персональная
                        информация, пока такая информация будет являться совмещённой.</p>
                    <ol start="4">
                        <li>Передача персональных данных</li>
                    </ol>
                    <p>Персональные данные Пользователей не передаются каким-либо третьим лицам, за исключением случаев,
                        прямо предусмотренных настоящими Правилами. Обработка персональных данных Пользователя
                        осуществляется без ограничения срока, любым законным способом, в том числе в информационных
                        системах персональных данных с использованием средств автоматизации или без использования таких
                        средств. Пользователь соглашается с тем, что Оператор вправе передавать персональные данные
                        третьим лицам, в частности, курьерским службам, организациями почтовой связи, операторам
                        электросвязи и т.д., исключительно для целей, указанных в разделе «Сбор персональных данных»
                        настоящей Политики конфиденциальности. При указании пользователя или при наличии согласия
                        пользователя возможна передача персональных данных Пользователя третьим лицам-контрагентам
                        Оператора с условием принятия такими контрагентами обязательств по обеспечению
                        конфиденциальности полученной информации, в частности, при использовании приложений. Приложения,
                        используемые Пользователями на Сайте, размещаются и поддерживаются третьими лицами
                        (разработчиками), которые действуют независимо от Оператора и не выступают от имени или по
                        поручению Оператора. Пользователи обязаны самостоятельно ознакомиться с правилами оказания услуг
                        и политикой защиты персональных данных таких третьих лиц (разработчиков) до начала использования
                        соответствующих приложений. Персональные данные Пользователя могут быть переданы по запросам
                        уполномоченных органов государственной власти РФ только по основаниям и в порядке, установленным
                        законодательством РФ. Оператор осуществляет блокирование персональных данных, относящихся к
                        соответствующему Пользователю, с момента обращения или запроса Пользователя или его законного
                        представителя либо уполномоченного органа по защите прав субъектов персональных данных на период
                        проверки, в случае выявления недостоверных персональных данных или неправомерных действий.</p>
                    <p>Раскрытие информации третьим лицам</p>
                    <p>Для использования персональных данных для любой иной цели Компания запрашивает у Пользователя
                        Согласие на обработку его персональных данных.</p>
                    <p>Поставщики услуг</p>
                    <p>Компания предоставляет персональные данные/информацию Компаниям, оказывающим такие услуги, как:
                        обработка информации, предоставление кредитов, исполнение заказов потребителей, доставка, иные
                        виды обслуживания потребителей, определение интереса Пользователя к продуктам и услугам,
                        проведение опросов, направленных на изучение потребителей или удовлетворения качеством сервиса.
                        Такие компании обязуются защищать информацию Пользователя независимо от страны своего
                        расположения.</p>
                    <p>Иные лица</p>
                    <p>Компании может быть необходимо — в соответствии с законом, судебным порядком, в судебном
                        разбирательстве и/или на основании публичных запросов или запросов от государственных органов на
                        территории или вне территории страны пребывания Пользователя — раскрыть персональные данные.
                        Компания может раскрывать персональные данные/информацию о Пользователе, если определит, что
                        такое раскрытие необходимо или уместно в целях национальной безопасности, поддержания
                        правопорядка или иных общественно важных случаях. Компания также может раскрывать персональные
                        данные/информацию о Пользователе, если определит, что раскрытие необходимо для приведения в
                        исполнение положений и условий Компании либо для целей защиты деятельности и пользователей.
                        Дополнительно в случае реорганизации, слияния или продажи Компания может передать любую или всю
                        собираемую персональную информацию соответствующему третьему лицу.</p>
                    <ol start="5">
                        <li>Уничтожение персональных данных</li>
                    </ol>
                    <p>Персональные данные пользователя уничтожаются при:</p>
                    <ul>
                        <li>самостоятельном удалении Пользователем данных со своей персональной страницы (если такие
                            страницы предусмотрены функциональностью сайта) с использованием функциональной возможности
                            «удалить аккаунт», доступной Пользователю при помощи настроек профиля;
                        </li>
                        <li>удалении Оператором информации, размещаемой Пользователем, а также персональной страницы
                            Пользователя в случаях, установленных договором купли продажи (оферта);
                        </li>
                        <li>при отзыве субъектом персональных данных согласия на обработку персональных данных.</li>
                    </ul>
                    <p>Идентификационные файлы (cookies) и иные технологии</p>
                    <p>Веб-сайт, интерактивные услуги и приложения, сообщения электронной почты и любые иные
                        коммуникации от лица Компании могут использовать идентификационные файлы cookies1 и иные
                        технологии, такие как: пиксельные ярлыки (pixel tags), веб-маяки (web beacons). Такие технологии
                        помогают Компании лучше понимать поведение пользователей, сообщают, какие разделы сайта были
                        посещены пользователями, и измеряют эффективность рекламы и сетевых поисков. Компания
                        рассматривает информацию, собираемую файлами cookies и иными технологиями как информацию, не
                        являющуюся персональной. Целью Компании в таких случаях является обеспечение более удобного и
                        персонального взаимодействия с Компанией. Например, зная имя Пользователя, Компания может
                        использовать его в своих коммуникациях. Зная, что кто-либо, используя компьютер или устройство
                        Пользователя, приобрел определённый продукт или воспользовался определенной услугой, Компания
                        может обеспечивать более полное соответствие интересам Пользователя рекламных сообщений и
                        сообщений электронной почты. В конечном итоге все эти знания помогают предоставлять
                        Пользователям обслуживание высшего качества. Пользователь может отключить cookies в настройках
                        используемого им веб-браузера или мобильного устройства. При этом следует учесть, что некоторые
                        функции веб-сайта могут стать недоступными после отключения cookies. Как и в случае большинства
                        веб-сайтов, Компания собирает некоторую информацию автоматически и хранит её в файлах
                        статистики. Такая информация включает в себя адрес Интернет-протокола (IP-адрес), тип и язык
                        браузера, информацию о поставщике Интернет-услуг, страницы отсылки и выхода, сведения об
                        операционной системе, отметку даты и времени, а также сведения о посещениях. Компания использует
                        такую информацию для понимания и анализа тенденций, администрирования сайта, изучения поведения
                        пользователей на сайте и сбора демографической информации 1 На сайте www.energomir.su
                        используются куки (cookies). Куки — это небольшие файлы данных, которые сохраняются на
                        компьютере или устройстве Пользователя при посещении веб-сайта. С их помощью веб-браузер
                        запоминает информацию, которая позволяет упростить работу с сайтом и сделать его более удобным
                        для каждого пользователя. Информация о том, как и когда Пользователь посещает сайт Компании,
                        помогает улучшить его. Создание файла куки возможно только в том случае, если это разрешено в
                        настройках веб-браузера. В любом браузере файлы куки доступны только для того веб-сайта, который
                        является их создателем. Другие веб-сайты не будут иметь доступа к этим файлам. На сайте
                        используются куки Яндекс. Метрики и Google Analytics, которые определяют сессию посетителя,
                        идентифицируют уникального посетителя, отслеживают источник трафика и навигацию. Также
                        используются куки для поддержания сеанса связи с пользователем и/или запуска специальной
                        анимации при первом посещении сайта. Домен Компании также может включать в себя элементы,
                        которые устанавливают куки от третьих лиц. Выбор использования файлов куки Пользователь может
                        настроить браузер для получения всех файлов куки, для отказа от всех файлов куки или для выдачи
                        уведомления при создании файла куки. Настройки каждого из браузеров различаются, поэтому
                        обратитесь к меню «Помощь» или «Настройки» своего браузера, чтобы узнать о способах изменения
                        настроек обработки файлов куки. об основном контингенте пользователей в целом. Компания может
                        использовать такую информацию в своих маркетинговых целях. В некоторых сообщениях электронной
                        почты Компания может использовать интерактивные ссылки на информацию, размещённую на сайте
                        Компании. Когда пользователи проходят по таким ссылкам, прежде чем они попадают на страницу
                        назначения на веб-сайте Компании, их запросы проходят отдельную регистрацию. Компания может
                        отслеживать такие «проходные» данные, для того чтобы помочь себе определить интерес к отдельным
                        темам и измерить эффективность коммуникаций с потребителями. Если Пользователь предпочитает,
                        чтобы обращения не отслеживались подобным образом, Пользователь не должен проходить по текстовым
                        или графическим ссылкам в сообщениях электронной почты. Пиксельные ярлыки позволяют Компании
                        направлять сообщения электронной почты в формате, читаемом потребителями, и сообщают, были ли
                        такие сообщения прочитаны. Компания может использовать такую информацию для ограничения
                        количества направляемых потребителям сообщений или прекращения их направления.</p>
                    <ol start="6">
                        <li>Защита персональных данных</li>
                    </ol>
                    <p style="margin-left:5.05pt">Компания предпринимает меры предосторожности — включая правовые,
                        организационные, административные, технические и физические — для обеспечения защиты
                        персональных данных Пользователя в соответствии со ст. 19 Федерального закона от 27.07.2006 N
                        152-ФЗ «О персональных данных» в целях обеспечения защиты персональных данных Пользователя от
                        неправомерного или случайного доступа к ним, уничтожения, изменения, блокирования, копирования,
                        распространения, а также от иных неправомерных действий третьих лиц.</p>
                    <p>Когда Пользователь использует некоторые продукты, услуги или приложения Компании или размещает
                        записи на форумах, в чатах или социальных сетях, предоставляемые персональные данные видны
                        другим пользователям и могут быть прочитаны, собраны или использованы ими. В таких случая
                        Пользователь несёт ответственность за персональные данные, которые предпочитает предоставлять,
                        самостоятельно. Например, если Пользователь указывает своё имя и адрес электронной почты в
                        записи на форуме, такая информация является публичной.</p>
                    <p>Пожалуйста, соблюдайте меры предосторожности при использовании таких функций.</p>
                    <br>
                    <p>Целостность и сохранение персональной информации</p>
                    <p style="margin-left:5px">Взаимодействуя с Компанией, Пользователь может легко поддерживать свои
                        персональные данные и информацию в актуальном состоянии. Компания будет хранить персональные
                        данные Пользователя и информацию в течение срока, необходимого для выполнения целей, описываемых
                        в настоящей Политике Конфиденциальности, за исключением случаев, когда более длительный период
                        хранения данных и информации необходим в соответствии с законодательством либо разрешён им.</p>
                    <p>Сторонние сайты и услуги</p>
                    <p style="margin-left:5.05pt">Веб-сайты, продукты, приложения и услуги Компании могут содержать
                        ссылки на веб-сайты, продукты и услуги третьих лиц. Продукты и услуги Компании могут также
                        использовать или предлагать продукты или услуги третьих лиц. Персональные данные и информация,
                        собираемая третьими лицами, которые могут включать такие сведения, как данные местоположения или
                        контактная информация, регулируется правилами соблюдения конфиденциальности таких третьих лиц.
                        Компания призывает Пользователя изучать правила соблюдения конфиденциальности таких третьих
                        лиц.</p>
                    <p>Оператор не несет ответственности за действия третьих лиц, получивших в результате использования
                        Интернета или Услуг Сайта доступ к информации о Пользователе и за последствия использования
                        данных и информации, которые, в силу природы Сайта, доступны любому пользователю сети
                        Интернет.</p>
                    <br>
                    <p>Соблюдение конфиденциальности Пользователя на уровне Компании</p>
                    <p style="margin-left:5.05pt">Для того чтобы убедиться, что персональные данные Пользователя
                        находятся в безопасности, Компания доводит нормы соблюдения конфиденциальности и безопасности до
                        работников Компании и строго следим за исполнением мер соблюдения конфиденциальности внутри
                        Компании.</p>
                    <p>&nbsp;</p>
                    <p>Вопросы относительно конфиденциальности</p>
                    <p style="margin-left:5.05pt">Если у Пользователя возникнут вопросы в отношении Политики
                        Конфиденциальности Компании или обработки данных Компанией, Пользователь может связаться с
                        Компанией по контактам для обратной связи.</p>
                    <br>
                    <ol start="7">
                        <li>Обращения пользователей</li>
                    </ol>
                    <p style="margin-left:5.05pt">К настоящей Политике конфиденциальности и отношениям между
                        Пользователем и Оператором применяется действующее законодательство РФ.</p>
                    <p>Пользователи вправе направлять Оператору свои запросы, в том числе запросы относительно
                        использования их персональных данных, направления отзыва согласия на обработку персональных
                        данных в письменной форме по адресу, указанному разделе Общие положения настоящего положения,
                        или в форме электронного документа, подписанного квалифицированной электронной подписью в
                        соответствии с законодательством РФ, и отправленного по контактам для обратной связи.</p>
                    <p>Запрос, направляемый Пользователем, должен соответствовать требованиям, установленным Правилами
                        подачи обращений в Службу сервиса и поддержки, а именно содержать:</p>
                    <ul>
                        <li>номер основного документа, удостоверяющего личность пользователя или его представителя;</li>
                        <li>сведения о дате выдачи указанного документа и выдавшем его органе;</li>
                        <li>сведения, подтверждающие участие пользователя в отношениях с Оператором (в частности, номер
                            заказа);
                        </li>
                        <li>подпись Пользователя или его представителя;</li>
                        <li>адрес электронной почты;</li>
                        <li>контактный телефон.</li>
                    </ul>

                    <p style="margin-left:5px">Оператор обязуется рассмотреть и направить ответ на поступивший запрос
                        Пользователя в течение 30 дней с момента поступления обращения.</p>
                    <br>
                    <p>Другое</p>
                    <p style="margin-left:5px">Во всем остальном, что не отражено напрямую в Политике
                        Конфиденциальности, Компания обязуется руководствоваться нормами и положениями Федерального
                        закона от 27.07.2006 N 152-ФЗ «О персональных данных»</p>
                    <p>Посетитель сайта Компании, предоставляющий свои персональные данные и информацию, тем самым
                        соглашается с положениями данной Политики Конфиденциальности.</p>
                    <p>Компания оставляет за собой право вносить любые изменения в Политику в любое время по своему
                        усмотрению с целью дальнейшего совершенствования системы защиты от несанкционированного доступа
                        к сообщаемым Пользователями персональным данным без согласия Пользователя. Когда Компания вносит
                        существенные изменения в Политику Конфиденциальности, на сайте размещается соответствующее
                        уведомление вместе с обновлённой версией Политики Конфиденциальности.</p>
                    <p>Действие настоящей Политики не распространяется на действия и интернет-ресурсов третьих лиц.</p>
                    <br>
                    <p>ООО «Мир горелок»</p>
                </div>
            </div>

            <?php include '../partials/goods.php' ?>
        </div>
    </div>

<?php include '../partials/footer.php' ?>