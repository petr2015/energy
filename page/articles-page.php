<?php include '../partials/header.php' ?>
    <div class="content">
        <div class="indent">
            <ul class="breadCrumb">
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span> Главная</a>
                </li>
                <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span>Полезная
                        информация по оборудованию интернет-магазина Энергомир</a></li>
                <li class="breadCrumb-li">Наши работы</li>
            </ul>
            <article>
                <h3 class="bold">Какой лучше купить настенный двухконтурный газовый котел</h3>
                <div class="article">
                    <p><img alt="Газовый двухконтурный настенный котел Bosch Gaz 6000 W" height="300"
                            src="/assets/img/1.png"
                            style="float:right" title="Газовый двухконтурный настенный котел Bosch Gaz 6000 W"
                            width="300">Сегодня на рынке отопительного оборудования существует огромное множество
                        различных устройств, которые используются в системах автономного отопления. Газовый
                        двухконтурный котел отопления настенной установки – одна из широко распространенных
                        разновидностей газовых котлов.</p>

                    <h2>Для чего нужен газовый двухконтурный настенный котел?</h2>

                    <p>Покупают газовый настенный двухконтурный котел для отопления и ГВС – данный тип котлов способен
                        обеспечить помещение как теплом, так и горячей водой. Он устанавливается на стену и отличается
                        компактностью – не требует отдельного помещения, его можно устанавливать на кухне и в ванной. <a
                                href="#">Купить
                            настенный двухконтурный газовый котел</a> в нашем интернет-магазине можно по выгодной цене.
                    </p>

                    <h3>Какие существуют газовые настенные двухконтурные котлы?</h3>

                    <p>Двухконтурные котлы чаще всего классифицируют по типу теплообменника для горячей воды: бывают
                        котлы с раздельным теплообменником, совмещенным или со встроенным бойлером.</p>

                    <ul>
                        <li><strong>Совмещенный «битермический» теплообменник</strong> представляет собой две
                            металлические трубки сложной формы, вставленные одна в другую. Во внутренней трубке
                            подогревается вода для ГВС, в наружной — для системы отопления. Рекомендуется использовать в
                            системах с мягкой водой. Такие котлы дешевле, но в случае засорения теплообменника накипью,
                            ремонт может обойтись до 50% стоимости котла.
                        </li>
                        <li><strong>Раздельный теплообменник состоит из двух отдельных модулей — первичного и
                                пластинчатого</strong>. Первичный теплообменник - это медная труба сложной формы с
                            оребрением, в нем нагревается теплоноситель из системы отопления; вторичный — набор пластин,
                            где подогревается вода для бытовых нужд. Располагаются они в разных частях котла — вверху
                            первичный, пластинчатый внизу, но связаны между собой. Такие котлы немного дороже, но более
                            ремонтопригодны. Замена любой из теплообменников стоит в 2-3 раза дешевле, чем совмещенного.
                        </li>
                        <li><strong>Встроенный бойлер </strong>- настенные газовые котлы с бойлером готовят горячую воду
                            для нужд отопления и для горячего водоснабжения во встроенном накопительном баке. <a
                                    href="#">Купить
                                настенный газовый котел с бойлером</a> можно у нас.
                        </li>
                    </ul>

                    <p>Кроме того, двухконтурные настенные котлы различаются по типу установленной горелки и камеры
                        сгорания.&nbsp;</p>

                    <ul>
                        <li><strong>Настенный двухконтурный &nbsp;газовый котел с открытой камерой сгорания оснащается
                                атмосферной горелкой</strong>. Воздух к таким горелкам поступает самотеком, забираясь
                            прямо из помещения, в котором установлено оборудование. Что касается продуктов сгорания, то
                            они удаляются через обычный дымоход с естественной тягой. Такие модели сжигают кислород в
                            помещении, но позволяют котлу работать при низком давлении газа и в широком диапазоне
                            нагрузок.&nbsp;
                        </li>
                        <li><strong>В настенных газовых двухконтурных котлах с закрытой камерой сгорания используются
                                вентиляторные (турбированные) горелки</strong>. Воздух для их работы забирается снаружи
                            зданий, через специальный двухслойный (коаксиальный) дымоход. Через этот же дымоход
                            осуществляется дымоудаление. Такие устройства отличаются высоком КПД за счет принудительной
                            подачи воздуха в камеру сгорания топлива при помощи вентиляторов.
                        </li>
                    </ul>

                    <h3>Как выбрать настенный двухконтурный газовый котел по мощности?</h3>

                    <p>Чаще и проще всего мощность можно выбрать, опираясь на площадь или объём помещения. Для жилых
                        домов высотой потолка до 3 метров для отопления 10 м2 надо 1 кВт тепла, для коммерческих и
                        промышленных зданий– 1 кВт для отопления 30 м3.</p>

                    <p>Если котел отопления будет готовить также и горячую воду, то к мощности добавляют 20% (умножить
                        на 1,2).</p>

                    <p>Например, если вам нужно подобрать котел для дома площадью 200 м2, то расчет будет такой:</p>

                    <p>Q = 200/10 = 20 кВт.</p>

                    <p>А если он должен готовить и горячую воду, то 20 кВт надо умножить на 1,2.</p>

                    <p>Q = 20*1.2 = 24 кВт.</p>

                    <p>Это и будет его максимальная полезная мощность, которая Вам нужна для подбора.</p>

                    <p>Эти цифры могут значительно изменяться в зависимости от качества утепления дома, его состояния,
                        площади остекления, вентиляции, количества точек водоразбора и других параметров.</p>

                    <p>Если вам нужна помощь в расчетах –&nbsp;<a href="#">позвоните нам</a>&nbsp;или оставьте заявку на
                        звонок.</p>

                    <h4>Преимущества двухконтурных газовых настенных котлов</h4>

                    <ul>
                        <li>Компактность. Такой отопительный котел обладает небольшими габаритами и весом, не требует
                            специального помещения и легко вписывается в интерьер&nbsp;благодаря современному дизайну.
                        </li>
                        <li>Возможность с помощью одного устройства обеспечивать и обогрев помещений, и подогрев воды.
                        </li>
                        <li>Экономность - значительное сокращение расходов по коммунальным платежам (холодная вода
                            обходится значительно дешевле горячей).
                        </li>
                        <li>Низкая цена по сравнению с другими типами устройств для автономного отопления.</li>
                        <li>Безопасность и удобное управление. Современные настенные котлы газовые двухконтурные чаще
                            всего оснащены системой безопасности, нередко - многоступенчатой, которая предохраняет котлы
                            от перегрева, например, при отключении насоса, затухания, если неожиданно прекращена подача
                            газа, и других возможных проблем. Кроме того, современный двухконтурный газовый котел, как
                            правило, оснащен электронной системой, поддерживающей определенную температуру, а некоторые
                            котлы укомплектованы также внешним датчиком температуры и в автоматическом режиме
                            поддерживают интенсивность отопления.<br>
                            <a href="#">Интернет-магазин отопительного оборудования «Энергомир»</a> предлагает большой
                            выбор настенных&nbsp;двухконтурных газовых котлов&nbsp;для квартиры и частного дома от
                            ведущих европейских и отечественных производителей.
                        </li>
                    </ul>
                </div>
            </article>
            <h3 class="bold">Задайте вопрос нашим специалистам и получите ответ прямо здесь</h3>
            <div class="pd-mb bold">Чтобы оставлять комментарии необходимо войти</div>
            <div class="work-seti">
                <div class="text">Войти с помощью:</div>
                <div id="uLogin"
                     data-ulogin="display=small;theme=classic;fields=first_name,last_name;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2F;mobilebuttons=0;"></div>
            </div>
            <?php include '../partials/goods.php' ?>
        </div>
    </div>

<?php include '../partials/footer.php' ?>