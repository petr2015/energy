<?php include '../../../partials/header.php' ?>
    <div class="content">
        <div class="indent">
            <div class="wrap-sidebar-content">
                <div class="sort">
                    <ul class="breadCrumb">
                        <li class="breadCrumb-li"><a href="#" class="breadCrumb-link"><span class="arrow"></span>Главная</a>
                        </li>
                        <li class="breadCrumb-li">Бойлеры</li>
                    </ul>
                    <h3 class="bold">Котлы отопления</h3>

                    <div class=" subtextH3">
                        <p>Котлы для частных домов и промышленных помещений в интернет-магазине «Энергомир».
                            Оборудование для котельных. Бесплатная консультация технического специалиста. Помощь в
                            подборе. Продажа и доставка оборудования по всей России. <a href="#hd">Читать далее</a></p>
                    </div>

                    <div class="subcategories-card">
                        <div class="card">
                            <a href="/page/cataloge/boilers/boilers.php" class="absLink"></a>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <div class="special-text">Газовые</div>
                        </div>
                        <div class="card">
                            <a href="/page/cataloge/boilers/boilers.php" class="absLink"></a>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <div class="special-text">Электрические</div>
                        </div>
                        <div class="card">
                            <a href="/page/cataloge/boilers/boilers.php" class="absLink"></a>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <div class="special-text">Косвенного нагрева</div>
                        </div>
                    </div>

                    <a href="#" class="sort-imgLink">
                        <img class="sort-imgLink-img" alt="Выбрать котел. Сколько стоит отопление?"
                             title="Выбрать котел. Сколько стоит отопление?"
                             src="/assets/img/kotel-cat2.jpg">
                    </a>

                    <div class="category-tag">
                        <div class="h4 bold">Быстрый выбор бойлера</div>
                        <ul class="category-tag-ul">
                            <li class="category-tag-li"><a href="#" class="category-tag-link">
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                    Газовые колонки
                                </a>
                            </li>
                            <li class="category-tag-li"><a href="#" class="category-tag-link">
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                    Накопительные водонагреватели
                                </a>
                            </li>
                            <li class="category-tag-li"><a href="#" class="category-tag-link">
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                    Проточные водонагреватели
                                </a>
                            </li>
                        </ul>
                    </div>

                    <?php include '../../../partials/aside.php' ?>

                    <div class="fastKons ">
                        <div class="flex-jb-ac-fw row">
                            <div class="col-xs-12 col-md-6">
                                <div class="bold fs14">Есть вопросы? - напишите ваш номер:</div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="fastKons-form">
                                    <form action="#" method="" class="callMeForm">
                                        <div class="inp">
                                            <input type="text" class="phone" name="" placeholder="+7 (___) ___-__-__">
                                            <button class="btn green" type="submit">
                                                <span class="fa fa-phone"></span> Перезвоните
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sortNav">
                        <div class="sortNav-btn">
                            <div class="sortNav-btn-click active"><i class="fa fa-th" aria-hidden="true"></i></div>
                            <div class="sortNav-btn-click "><i class="fa fa-th-list" aria-hidden="true"></i></div>
                        </div>
                        <div class="sortNav-prise">
                            <a href="#" class="dashed active">Сначала дорогие <i class="fa fa-chevron-down"
                                                                                 aria-hidden="true"></i></a>
                            <a href="#" class="dashed ">Сначала дешевые <i class="fa fa-chevron-up"
                                                                           aria-hidden="true"></i></a>
                            <a href="#" class="dashed active">Сбросить <i class="fa fa-times"
                                                                          aria-hidden="true"></i></a>
                        </div>
                        <div class="sortNav-rig">
                            <span>Показано 7 из 7</span>
                        </div>
                    </div>
                    <div class="sort-card">
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <div class="card-model-yet fs12">
                                    <div class="bold black">Модели:</div>
                                    <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000 р.
                                    <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800 р.
                                    <div class="yet-box">
                                        <a href="/page/card/card.php" class="black">Еще (7)</a>
                                        <div class="card-model-open">
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                            р.<br>
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                            р.<br>
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                            р.<br>
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                            р.<br>
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                            р.<br>
                                            <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207
                                            100 р.<br>
                                            <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                        </div>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                        <div class="card">
                            <a href="/page/card/card.php" class="absLink"></a>
                            <div class="product-id grey"><span class="bold">ID</span>: 1359</div>
                            <div class="card-img">
                                <img src="/assets/img/img2.png" title="" alt="">
                            </div>
                            <a href="/page/card/card.php" class="card-link">Бойлер косвенного нагрева Galmet SOL
                                PARTNER</a>
                            <div class="product-price">
                                <div class="db bashed">
                                    <span>от</span>
                                    <span class="bold fs21">57 000 р.</span><br>
                                    <span class="fs12 grey">до</span>
                                    <span class="grey">274 600 р.</span>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Объём:</div>
                                    <div class="dib">от 200.00 до 1500.00 литров</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Монтаж:</div>
                                    <div class="dib">напольный</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Производительность:</div>
                                    <div class="dib">до 19.10 л/мин</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Конструкция:</div>
                                    <div class="dib">змеевик</div>
                                </div>
                                <div class="db fs12 brd">
                                    <div class="bold dib">Нагрев:</div>
                                    <div class="dib">комбинированный</div>
                                </div>
                            </div>
                            <div class="flex-jb-ac-fw cardBtn">
                                <div class="card-model btn grey fs12">
                                    <a href="/page/card/card.php" class="bold black"><i class="fa fa-bars"
                                                                                        aria-hidden="true"></i>
                                        Модели</a>
                                    <div class="card-model-open">
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 200</a> - 57 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 300</a> - 63 800
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 400</a> - 78 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 500</a> - 100 000
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 720</a> - 165 100
                                        р.<br>
                                        <a href="/page/card/card.php" class="grey brb">SOL PARTNER 1000</a> - 207 100 р.<br>
                                        <a href="/page/card/card.php" class="bold grey fs12">Посмотреть все</a>
                                    </div>
                                </div>
                                <a href="/page/card/card.php" class="btn green fs12">Подробнее</a>
                            </div>
                        </div>
                    </div>

                    <div class="wrap-pagination">
                        <ul class="pagination">
                            <li class="pagination-li"><a class="pag-link active" href="#">1</a></li>
                            <li class="pagination-li"><a class="pag-link" href="#">2</a></li>
                            <li class="pagination-li"><a class="pag-link" href="#">3</a></li>
                            <li class="pagination-li"><a class="pag-link" href="#">4</a></li>
                            <li class="pagination-li"><a class="pag-link" href="#">5</a></li>
                            <li class="pagination-li"><em>...</em></li>
                            <li class="pagination-li"><a class="pag-link" href="#"><i class="fa fa-angle-right"
                                                                                      aria-hidden="true"></i></a></li>
                            <li class="pagination-li"><a class="pag-link" href=""><i class="fa fa-angle-double-right"
                                                                                     aria-hidden="true"></i></a></li>
                        </ul>
                    </div>

                    <div id="hd" class="category-description">
                        <p>Если в доме или квартире нет централизованного отопления, автономная система,
                            работающая от котла, эффективно обогреет помещение, а также обеспечит подготовку горячей
                            воды.</p>
                        <p>Выбор отопительного котла зависит в первую очередь от объема отапливаемого объекта
                            и используемого топлива. Выделяют газовые, электрические, твердотопливные и жидкотопливные
                            агрегаты. Для отопления покупают одноконтурные модели, для отопления и ГВС -
                            двухконтурные.</p>
                        <p>Для промышленной котельной больше подойдут <a href="#">промышленные водогрейные котлы </a>
                            - высокоэффективные и полностью безопасные устройства, предназначенные для
                            нагрева воды под давлением.</p>
                        <p>В зависимости от установки различают настенные и напольные модели котлов.</p>
                        <p>Подробнее о том, <a href="https://energomir.su/kak-vybrat-kotel.html">как выбрать котел</a></p>
                        <p>Мы продаем оборудование только надежных и проверенных брендов. У нас можно купить котлы:</p>
                        <div class="brandHorizontal">
                            <div class="table-responsive">
                                <table border="1" cellpadding="1" cellspacing="1" class="table table-condensed" style="width:100%">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div><a href="#"><img alt="Котлы Baxi" height="40" src="/assets/img/b1.jpg" title="Котлы Baxi"> BAXI </a></div>

                                            <div><a href="#"><img alt="Котлы Alphatherm" height="35" src="/assets/img/b2.jpg" title="Котлы Alphatherm"> Alphatherm</a></div>

                                            <div><a href="#"><img alt="Котлы Alphatherm" height="35" src="/assets/img/b3.jpg" title="Котлы Alphatherm"> Beretta </a></div>

                                            <div><a href="#"><img alt="Котлы Buderus" height="35" src="/assets/img/b4.jpg" title="Котлы Buderus"> Buderus </a></div>

                                            <div><a href="#"><img alt="Котлы Lamborgini" height="35" src="/assets/img/b5.jpg" title="Котлы Lamborgini"> Lamborghini </a></div>

                                            <div><a href="#"><img alt="Котлы Protherm" height="35" src="/assets/img/b6.jpg" title="Котлы Protherm"> Protherm </a></div>
                                        </td>
                                        <td>
                                            <div><a href="#"><img alt="Котлы Vaillant" height="35" src="/assets/img/b7.jpg" title="Котлы Vaillant"> Vaillant </a></div>

                                            <div><a href="#"><img alt="Котлы Ferroli" height="35" src="/assets/img/b8.jpg" title="Котлы Ferroli"> Ferroli </a></div>

                                            <div><a href="#"><img alt="Котлы Chaffoteaux" height="35" src="/assets/img/b9.jpg" title="Котлы Chaffoteaux"> Chaffoteaux </a></div>

                                            <div><a href="#"><img alt="Котлы Daewoo" height="35" src="/assets/img/b10.jpg" title="Котлы Daewoo"> Daewoo </a></div>

                                            <div><a href="#"><img alt="Котлы Rinnai" height="35" src="/assets/img/b11.jpg" title="Котлы Rinnai"> Rinnai </a></div>

                                            <div><a href="#"><img alt="Котлы Kiturami" height="35" src="/assets/img/b12.jpg" title="Котлы Kiturami"> Kiturami</a></div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include '../../../partials/goods.php' ?>
        </div>
    </div>
<?php include '../../../partials/footer.php' ?>