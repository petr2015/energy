<?php include 'partials/header.php' ?>

<div class="content">
    <div class="indent">

        <div class="specialOffer">
            <div class="specialOffer-lef">
                <div class="wrap-title">
                    <div class="title">Специальное предложение</div>
                </div>
                <div class="wrap-offer">
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Газовый котел Arderia B</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 25 080 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Бойлер косвенного нагрева Galmet TOWER</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 27 300 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Котел на отработанном масле Sime + Euronord</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 126 585 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Горелка на отработанном масле Master MB</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 117 680 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Электрокотел УралПром ЭВПМ</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 3 175 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                    <div class="offerCard flex-dc-ac-jb">
                        <a href="#" class="offerCard-tit">Дизельная горелка Lamborghini ECO</a>
                        <div class="offerCard-img">
                            <img src="/assets/img/img1.jpg" title="" alt="">
                        </div>
                        <div class="offerCard-price">
                            <div class="offerCard-price-text">От 28495 р.</div>
                            <a class="offerCard-price-link" href="#">Подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="specialOffer-rig">
                <div class="wrap-h">
                    <h4>НаШИ ПРЕИМУЩЕСТВА</h4>
                </div>
                <div class="wrap-adv">
                    <div class="adv-img">
                        <img src="/assets/img/delivery.png" title="" alt="">
                    </div>
                    <div class="adv-text">
                        <a href="#" class="adv-link">
                            <span class="bold">БЕСПЛАТНАЯ ДОСТАВКА</span>
                            <span>ПРИ ЗАКАЗЕ ОТ 8000 Р.</span>
                        </a>
                    </div>
                </div>
                <div class="wrap-adv">
                    <div class="adv-img">
                        <img src="/assets/img/cash.png" title="" alt="">
                    </div>
                    <div class="adv-text">
                        <div class="adv-link">
                            <span class="bold">ОПЛАТА ТОВАРА</span>
                            <span>ПОСЛЕ ДОСТАВКИ</span>
                        </div>
                    </div>
                </div>
                <div class="wrap-adv">
                    <div class="adv-img">
                        <img src="/assets/img/2000.png" title="" alt="">
                    </div>
                    <div class="adv-text">
                        <div class="adv-link">
                            <span class="bold">БОЛЕЕ 2000 ТОВАРОВ</span>
                            <span>В НАЛИЧИИ</span>
                        </div>
                    </div>
                </div>
                <div class="wrap-adv">
                    <div class="adv-img">
                        <img src="/assets/img/callme.png" title="" alt="">
                    </div>
                    <div class="adv-text">
                        <a href="#" class="adv-link">
                            <span class="bold">КОНСУЛЬТАЦИИ</span>
                            <span>И ПОМОЩЬ В ВЫБОРЕ</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="wrap-category">
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
            <div class="category-box">
                <div class="wrap-h">
                    <a href="#" class="category-link">Бойлеры</a>
                </div>
                <div class="category-card">
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img2.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Galmet TOWER</div>
                        <div class="card-price bold">От 27 300 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img3.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel WW TERMO HIT</div>
                        <div class="card-price bold">От 9 500 р.</div>
                    </div>
                    <div class="card">
                        <a href="#" class="absLink"></a>
                        <div class="card-img">
                            <img src="/assets/img/img4.png" title="" alt="">
                        </div>
                        <div class="card-link">Бойлер косвенного нагрева Kospel SN</div>
                        <div class="card-price bold">От 21 000 р.</div>
                    </div>
                </div>
                <div class="wrap-tag">
                    <a href="#" class="tag">БУФЕРНЫЕ ЕМКОСТИ</a>
                    <a href="#" class="tag">КОСВЕННОГО НАГРЕВА</a>
                    <a href="#" class="tag">ЭЛЕКТРИЧЕСКИЕ</a>
                </div>
            </div>
        </div>

        <div class="energyBox">
            <h3>Энергомир - интернет-магазин отопительного и климатического оборудования</h3>
            <div class="subtitle">Энергомир - интернет-магазин отопительного и климатического оборудования</div>
            <p>Если нужен <a href="#">котел отопления</a>,
                <a href="#">горелка</a>,
                <a href="#">бойлер</a>, другое тепловое или климатическое оборудование
                – Вы по адресу. У нас можно:
            </p>
            <ul>
                <li>Купить отопительную и климатическую технику с доставкой по России</li>
                <li>Получить консультацию по подбору устройств</li>
                <li>Сделать проект отопления</li>
                <li>Заказать установку котла или горелки и пуско-наладочные работы.</li>
            </ul>
            <p>Компания «Энергомир» –&nbsp; официальный дилер европейских компаний Lamborghini, Polykraft, Oso, Cib
                UNIGAS, ICI Caldaie, Galmet и других. Это позволяет нам предлагать качественное оборудование по
                адекватной цене.
                <br>
                <br>
                Связаться с нами по любым вопросам можно по телефонам, указанным <a href="#">здесь</a>.
            </p>
        </div>

        <?php include 'partials/goods.php' ?>

    </div>
</div>


<?php include 'partials/footer.php' ?>

