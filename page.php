<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="/assets/css/vendor.css">
    <link rel="stylesheet" href="/assets/css/style.css">

</head>
<body>


<div class="indent">
    <table border="1" class="page-table">
        <tr>
            <td><a href="index.php">главная</a></td>
            <td><a href="index.php">index.php</a></td
        </tr>
        <tr>
            <td><a href="page/card/card.php">Карта товара</a></td>
            <td><a href="page/card/card.php">card.php</a></td>
        </tr>
        <tr>
            <td><a href="page/card/card-brand.php">Карта товара, марка товара</a></td>
            <td><a href="page/card/card-brand.php">card-brand.php</a></td>
        </tr>
        <tr>
            <td><a href="page/card/compare-products.php">Сравнение товаров</a></td>
            <td><a href="page/card/compare-products.php">compare-products.php</a></td>
        </tr>
        <tr>
            <td><a href="page/card/compare-products-non.php">Сравнение товаров без товаров</a></td>
            <td><a href="page/card/compare-products-non.php">compare-products-non.php</a></td>
        </tr>
        <tr>
            <td><a href="page/basket.php">Корзина</a></td>
            <td><a href="page/basket.php">basket.php</a></td>
        </tr>
        <tr>
            <td><a href="page/cataloge/boilers/boilers.php">Бойлеры</a></td>
            <td><a href="page/cataloge/boilers/boilers.php">boilers.php</a></td>
        </tr>
        <tr>
            <td><a href="page/cataloge/waterHeater/water-heater.php">Водонагреватели</a></td>
            <td><a href="page/cataloge/waterHeater/water-heater.php">water-heater.php</a></td>
        </tr>
        <tr>
            <td><a href="page/cataloge/heaters/heaters.php">Водяные калориферы</a></td>
            <td><a href="page/cataloge/heaters/heaters.php">heaters.php</a></td>
        </tr>
        <tr>
            <td><a href="page/cataloge/heatingBoiler/heating-boiler.php">Котлы отопления</a></td>
            <td><a href="page/cataloge/heatingBoiler/heating-boiler.php">heating-boiler.php</a></td>
        </tr>
        <tr>
            <td><a href="page/production/production.php">Поиск товаров по производителю</a></td>
            <td><a href="page/production/production.php">production.php</a></td>
        </tr>
        <tr>
            <td><a href="page/production/insideProduction.php">Главная > Производители > Один из списка</a></td>
            <td><a href="page/production/insideProduction.php">insideProduction.php</a></td>
        </tr>
        <tr>
            <td><a href="page/work.php">Наши работы</a></td>
            <td><a href="page/work.php">work.php</a></td>
        </tr>
        <tr>
            <td><a href="page/articles.php">Статьи</a></td>
            <td><a href="page/articles.php">articles.php</a></td>
        </tr>
        <tr>
            <td><a href="page/articles-page.php">Переход на статьи</a></td>
            <td><a href="page/articles-page.php">articles-page.php</a></td>
        </tr>
        <tr>
            <td><a href="page/payment-delivery.php">Оплата и доставка</a></td>
            <td><a href="page/payment-delivery.php">payment-delivery.php</a></td>
        </tr>
        <tr>
            <td><a href="page/contact.php">Контакты</a></td>
            <td><a href="page/contact.php">contact.php</a></td>
        </tr>
    </table>
</div>



<script type="text/javascript" src="/assets/js/script.min.js"></script>
</body>
</html>