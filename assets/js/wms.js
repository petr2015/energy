$(document).ready(function () {

    $(window).scroll(function () {
        $('header').toggleClass('fix', $(this).scrollTop() > 87);
    });

    // menu +
    $(window).scroll(function () {
        if (document.documentElement.scrollTop > 87) {
            $('.wrapMenu').removeClass('open');}
        //  if (document.documentElement.scrollTop < 87) {
        //     $('.wrapMenu').addClass('open');
        // }
    });
    // menu -

    if (document.documentElement.clientWidth < 768) {
        $(window).scroll(function () {
            $('header').removeClass('fix');
        });
    }

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });
    $('.hamburger').click(function () {
        $('.menuPage').toggleClass('open');
    });

    $('.wrapMenu').click(function () {
        $(this).toggleClass('openMenu');
    });

    if (document.documentElement.clientWidth < 1200) {
        $('.wrapMenu').removeClass('open');
    }


    $(".phone").mask("+7 (999) 999-9999");


//    page basket +

    $(document).on('change','#client',function () {
        $('.form-basket-line.hidden').toggleClass('open');
    });


    $('.fa-times-click').click(function () {
        $(this).closest('.divTable-card').hide();
    });

    $('.owl-basket').owlCarousel({
        margin:10,
        nav:false,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            640:{
                items:2
            },
            768:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });

    $ ('.dashed-link').click(function () {
        $(this).closest('.basket-lef-bord').slideToggle();
    });
//    page basket -

//    производителт +
    $(function(){
        $("a[href^='#hd'], a[href^='#heaters'], a[href^='#cd']," +
            "a[href^='#01'],a[href^='#a'],a[href^='#b'],a[href^='#c'],a[href^='#d']," +
            "a[href^='#f'],a[href^='#g'],a[href^='#h'],a[href^='#i'],a[href^='#k']," +
            "a[href^='#l'],a[href^='#m'],a[href^='#n'],a[href^='#o'],a[href^='#p']," +
            "a[href^='#r'],a[href^='#s'],a[href^='#t'],a[href^='#u'],a[href^='#v']," +
            "a[href^='#w'],a[href^='#y'],a[href^='#z'],a[href^='#ar'],a[href^='#br']," +
            "a[href^='#vr'],a[href^='#gr'],a[href^='#dr'],a[href^='#kr'],a[href^='#lr']," +
            "a[href^='#mr'],a[href^='#nr'],a[href^='#or'],a[href^='#rr'],a[href^='#cr']," +
            "a[href^='#tr'],a[href^='#ur'],a[href^='#fr'],a[href^='#chr'],a[href^='#lr']," +
            "a[href^='#yr']").click(function(){
            var _href = $(this).attr("href");
            $("html, body").animate({scrollTop: $(_href).offset().top-"90"});
            return false;
        });
    });
//    производителт +

});