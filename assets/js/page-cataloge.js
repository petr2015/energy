$(document).ready(function () {
    $('.option-name').click(function () {
        $(this).closest('.filter-option').find('.filter-open').slideToggle();
        $(this).closest('.filter-option').toggleClass('open');
    });

    $('.property__caption').on('click', '.property-link:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.propertyTab').find('.property__content').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('.sortNav-btn').on('click', '.sortNav-btn-click:not(.active)', function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.sort-card').toggleClass('active');
    });

    // изменение параметров ползунком в сайтбаре +
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 50,
            max: 5000,
            values: [ 50, 5000 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "" + ui.values[ 0 ] );
                $( "#amount2" ).val("" + ui.values[ 1 ] );
            },
        });
        $( "#amount" ).val( "" + $( "#slider-range" ).slider( "values", 0 ) );
        $( "#amount2" ).val("" + $( "#slider-range" ).slider( "values", 1 ) );
    } );

    $( function() {
        $( "#slider-prise" ).slider({
            range: true,
            min: 10915,
            max: 1821750,
            values: [ 10915, 1821750 ],
            slide: function( event, ui ) {
                $( "#prise" ).val( "" + ui.values[ 0 ] );
                $( "#prise2" ).val("" + ui.values[ 1 ] );
            },
        });
        $( "#prise" ).val( "" + $( "#slider-prise" ).slider( "values", 0 ) );
        $( "#prise2" ).val("" + $( "#slider-prise" ).slider( "values", 1 ) );
    } );
    $( function() {
        $( "#slider-performance" ).slider({
            range: true,
            min: 3.00,
            max: 79.10,
            values: [ 3.00, 79.10 ],
            slide: function( event, ui ) {
                $( "#performance" ).val( "" + ui.values[ 0 ] );
                $( "#performance2" ).val("" + ui.values[ 1 ] );
            },
        });
        $( "#performance" ).val( "" + $( "#slider-performance" ).slider( "values", 0 ) );
        $( "#performance2" ).val("" + $( "#slider-performance" ).slider( "values", 1 ) );
    } );
    $( function() {
        $( "#slider-power" ).slider({
            range: true,
            min: 2,
            max: 312,
            values: [ 2, 312 ],
            slide: function( event, ui ) {
                $( "#power" ).val( "" + ui.values[ 0 ] );
                $( "#power2" ).val("" + ui.values[ 1 ] );
            },
        });
        $( "#power" ).val( "" + $( "#slider-power" ).slider( "values", 0 ) );
        $( "#power2" ).val("" + $( "#slider-power" ).slider( "values", 1 ) );
    } );
    // изменение параметров ползунком в сайтбаре -

    $('.sidebarBtn-click').click(function () {
        $('.sidebarForm').slideToggle();
    });
});