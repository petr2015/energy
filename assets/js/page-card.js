$(document).ready(function () {


    // card fancybox +
    $('.wrap-fancyBox').on('mouseover', '.photo', function () {
        var src = $(this).attr('src');
        $(this).parent().parent().parent().closest('.wrap-fancyBox').find('.fancyBox-max img').attr('src', src);
    });

    $('.fancybox').fancybox({
        padding : 300 ,
        arrows: true,
        openEffect  : 'elastic'
    });
    // card fancybox -

    //    card owlCarousel +
    $('.owl-model').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            992:{
                items:2
            }
        }
    });

    $('.owl-earlier').owlCarousel({
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:3000,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            640:{
                items:4
            },
            992:{
                items:6
            }
        }
    });

    $('.owl-compare').owlCarousel({
        margin:10,
        nav:false,
        dots: true,
        responsive:{
            0:{
                items:1
            },
            640:{
                items:2
            },
            768:{
                items:3
            },
            1200:{
                items:3
            }
        }
    });

    //    card owlCarousel -

    // tabs +
    $('.property__caption').on('click', '.property-link:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.propertyTab').find('.property__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    $('.p_cn').on('click', '.p_l:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('.pT').find('.p_ct').removeClass('active').eq($(this).index()).addClass('active');
    });

    $('.btnTab').click(function () {
        $('.btnTab').toggleClass('active');
        $('.table-hover').toggleClass('open');
    });
    // tabs -


    $(function () {

    });
    // высота в таблице на стр card +
    var table1_tr = $('.table1  tr');
    var table2_tr = $('.table2  tr');
    $.each(table2_tr, function (k, el) {
        if ($(el).height() > table1_tr.eq(k).height()){
            table1_tr.eq(k).css('height',$(el).height());
        }else{
            $(el).css('height',table1_tr.eq(k).height())
        }
    });
    // высота в таблице на стр card -

    // hover table page card +
    table2_tr.hover(function () {
        $(this).toggleClass('hov');
        table1_tr.eq($(this).index()).toggleClass('hov');
    });

    table1_tr.hover(function () {
        $(this).toggleClass('hov');
        table2_tr.eq($(this).index()).toggleClass('hov');
    });
    // hover table page card -

    //листать таблицу +
    $('.tableNext').on('click', function() {
        var tableNext = $('.tableProperty-wrap');
        tableNext.animate({scrollLeft:(tableNext.scrollLeft()+190)}, 200);
    });
    $('.tablePrev').on('click', function() {
        var tablePrev = $('.tableProperty-wrap');
        tablePrev.animate({scrollLeft:(tablePrev.scrollLeft()-190)}, 200 );
    });
    //листать таблицу -

//    работает при разрешении 1200 +
    $('.description h3.mobClick').click(function () {
        $('.description-inf').slideToggle();
    });
    $('.main-param h3.mobClick').click(function () {
        $('.mainParameters').slideToggle();
    });
//    работает при разрешении 1200 -

//    мобила аккордион +
    $('.parameterMob').click(function () {
        $(this).closest('.wrap-parameterMob').toggleClass('color');
        $(this).closest('.wrap-parameterMob').find('.parameterMob-open').slideToggle()
    });
//    мобила аккордион -

//    плавный скролл для якоря +
    $(function(){
        $("a[href^='#compare']").click(function(){
            var _href = $(this).attr("href");
            $("html, body").animate({scrollTop: $(_href).offset().top-"90"});
            return false;
        });
    });
//    плавный скролл для якоря -

    $('.analoguesBtn').click(function () {
        $('.slider-compare').slideToggle();
    });
});